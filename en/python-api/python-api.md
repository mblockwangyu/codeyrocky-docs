# Python API

This article includes the following python API for Codey Rocky:

- [Python API for Codey](codey-api.md)：refers to some API for Codey Rocky’s onboard driver.
- [Python API for Rocky](rocky-api.md)：API on Codey which used for control the Rocky to move or to receive dates send from sensors on Rocky.
- [Python API for third-party libraries](third-party-api.md)：some build-in interface for third-party libraries of Codey Rocky, such as `mqtt` or `urequest`.
- [Python API for extension Neuron modules](neuron-api.md)：API which may used when add Neuron modules to Codey Rocky.