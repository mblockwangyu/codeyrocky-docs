# Control Category

## 1. wait () seconds

Wait for a specified period of time to run the script.

![](images/control-1-1.png)

**Example:**

![](images/control-1-2.png)

Press Button A. One second later, Codey's screen will display the image.

---

## 2. repeat ()

Run the script for the specified number of times.

![](images/control-2-1.png)

**Example:**

![](images/control-2-2.png)

When Codey starts up, it will play the sound "hello" for ten times.

---

## 3. forever

Run the script repeatedly.

![](images/control-3-1.png)

**Example:**

![](images/control-3-2.png)

When Codey starts up, Codey's will repeated display the two images consecutively.

---

## 4. if () then ()

If the report condition is met, run the script.

![](images/control-4-1.png)

**Example:**

![](images/control-4-2.png)

When Codey starts up, if being shaken, Codey will play the sound "hello".

---

## 5. if () then () else ()

If the report condition is met, run script 1. If not, run script 2.

![](images/control-5-1.png)

**Example:**

![](images/control-5-2.png)

When Codey starts up, if there are obstacles ahead, Codey Rocky will stop moving. If not, Codey Rocky will keep moving forward at 50% power.

---

## 6. wait ()

Wait until the report condition is met. Run the script.

![](images/control-6-1.png)

**Example:**

![](images/control-6-2.png)

After starting up, Codey Rocky will keep moving forward at 50% power until Rocky detects obstacles ahead.

---

## 7. repeat until ()

Run the script repeatedly until the report condition is met.

![](images/control-7-1.png)

**Example:**

![](images/control-7-2.png)

After starting up, Codey Rocky will keep moving forward at 50% power until Rocky detects obstacles ahead.

---

## 8. stop ()

Stop the specified script or scripts. There are three options: all, this script, or other scripts in sprite.

![](images/control-8-1.png)

**Example:**

![](images/control-8-2.png)

Press Button A to stop all scripts.

---

