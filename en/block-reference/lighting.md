# Lighting Category

**RGB LED**

![](codey-led.png)

The RGB LED is situated at the bottom Codey's face panel. Codey's RGB LED consists of 3 LEDs, namely red LED, green LED, and blue LED. You can get different colors by controling brightness of each LED.

![](rocky-led.png)

At the center of Rocky's Color IR Sensor is the RGB LED, which consists of 3 LEDs, red LED, green LED, and blue LED. By lighting on or off each LED, you can get seven different colors.

## 1. RGB LED lights up () for () secs

Light up Codey's RGB LED a specified color for a specified period of time.

![](images/lighting-1-1.png)

**Example:**

![](images/lighting-1-2.png)

Press Button A, Codey's LED will light up red, and light off after 1 second.

---

## 2. RGB LED lights up ()

Light up Codey's RGB LED a specified color.

![](images/lighting-2-1.png)

**Example:**

![](images/lighting-2-2.png)

Press Button A, Codey's LED will light up red.

---

## 3. set the indicator () with color value ()

Set the specified LED indicator with specified color value. There are three LED indicators: red, green, and blue.

![](images/lighting-3-1.png)

Change the color value of one LED indicator will not affect the color value of the other two LED indicators.

**Parameters:**
- Red: 0 ~ 255
- Green: 0 ~ 255
- Blue: 0 ~ 255

**Example:**

![](images/lighting-3-2.png)

Press Button A, Codey's RGB LED will light up yellow.

---

## 4. RGB LED lights off

Turn off Codey's RGB LED.

![](images/lighting-4-1.png)

**Example:**

![](images/lighting-4-2.png)

Press Button A, Codey's RGB LED will light up yellow, and light off after 3 seconds.

---

## 5. set Rocky's light with color ()

Set Rocky's RGB LED a specified color. There are seven colors: red, green, blue, yellow, cyan, purple, and white.

![](images/lighting-5-1.png)

**Example:**

![](images/lighting-5-2.png)

Press Button A, Rocky's RGB LED will light up blue.

---

## 6. Rocky lights off

Turn off Rocky's RGB LED.

![](images/lighting-6-1.png)

**Example:**

![](images/lighting-6-2.png)

Press Button A, Rocky's RGB LED will light up blue, and light off after 2 seconds.

---