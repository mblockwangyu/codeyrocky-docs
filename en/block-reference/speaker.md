# Speaker Category

Codey's on-board speaker enables Codey to play sound. You can write music, add sound effects, and do other fun stuff with Codey's speaker.

## 1. play sound ()

Make Codey's speaker play a specified sound.

![](images/speaker-1-1.png)

<table cellpadding="10px;" cellspacing="12px;" style="text-align:center;">
<tr><th colspan="7" style="border: 1px solid black;">Sound Effect</th></tr>

<tr><td style="border: 1px solid black;">hello</td> <td style="border: 1px solid black;">hi</td> <td style="border: 1px solid black;">bye</td> <td style="border: 1px solid black;">yeah</td> <td style="border: 1px solid black;">wow</td> <td style="border: 1px solid black;">laugh</td> <td style="border: 1px solid black;">hum</td></tr>

<tr><td style="border: 1px solid black;">sad</td> <td style="border: 1px solid black;">sigh</td> <td style="border: 1px solid black;">annoyed</td> <td style="border: 1px solid black;">angry</td> <td style="border: 1px solid black;">scared</td> <td style="border: 1px solid black;">pettish</td> <td style="border: 1px solid black;">curious</td></tr>

<tr><td style="border: 1px solid black;">embarrassed</td> <td style="border: 1px solid black;">ready</td> <td style="border: 1px solid black;">sprint</td> <td style="border: 1px solid black;">snore</td> <td style="border: 1px solid black;">meow</td> <td style="border: 1px solid black;">start</td> <td style="border: 1px solid black;">switch</td></tr>

<tr><td style="border: 1px solid black;">beeps</td> <td style="border: 1px solid black;">buzz</td> <td style="border: 1px solid black;">air-out</td> <td style="border: 1px solid black;">explosion</td> <td style="border: 1px solid black;">gotcha</td> <td style="border: 1px solid black;">painful</td> <td style="border: 1px solid black;">jump</td></tr>

<tr><td style="border: 1px solid black;">laser</td> <td style="border: 1px solid black;">level-up</td> <td style="border: 1px solid black;">low-energy</td> <td style="border: 1px solid black;">metal-clash</td> <td style="border: 1px solid black;">prompt-tone</td> <td style="border: 1px solid black;">right</td> <td style="border: 1px solid black;">wrong</td></tr>

<tr><td style="border: 1px solid black;">ringtone</td><td style="border: 1px solid black;">score</td> <td style="border: 1px solid black;">shot</td> <td style="border: 1px solid black;">step_1</td> <td style="border: 1px solid black;">step_2</td> <td style="border: 1px solid black;">activate</td> <td style="border: 1px solid black;">warning</td> </tr>

</table>

<br>

**Example:**

![](images/speaker-1-2.png)

Press Button A. Codey will play the sound "hello".

---

## 2. play sound () until done

Make Codey's speaker play a specified sound, and wait until it's done.

![](images/speaker-2-1.png)

**Example:**

![](images/speaker-2-2.png)

Press Button A. Codey will play the sound "hello".

---

## 3. stop all sounds

Make Codey's speaker stop playing all sounds.

![](images/speaker-3-1.png)

**Example:**

![](images/speaker-3-2.png)

Press Button A. Codey will play the sound "hello" for 10 times, and then stop all sounds.

---

## 4. play note () for () beats

Make Codey's speaker play a specified note for specified number of beats. 

![](images/speaker-4-1.png)

 **Example:**

![](images/speaker-4-2.png)

Press Button A. Codey will play the note "C4" for 0.25 beat.

---

## 5. rest for () beats

Make Codey's speaker stop playing sounds and wait for specified number of beats.

![](images/speaker-5-1.png)

**Example:**

![](images/speaker-5-2.png)

Press Button A. Codey will play the note "C4" for 0.25 beat 5 times, and after waiting for 0.25 beat, play the note "G4" for 0.25 beat 5 times.

---

## 6. play sound at frequency of () HZ for () secs

Make Codey's speaker play sound at specified frequency for a specified period of time.

![](images/speaker-6-1.png)

**Example:**

![](images/speaker-6-2.png)

Press Button A. Cody will play a 700Hz sound for one second.

---

## 7. change volume by ()

Change the sound volmue of Codey's speaker. Input positive number to increase sound volume, or negative number to decrease sound volume.

![](images/speaker-7-1.png)

**Example:**

![](images/speaker-7-2.png)

Press Button A to decrease sound volume of Codey's speaker by 10.

---

## 8. set volume to ()%

Set the sound volume of Codey's speaker to specified percentage (0 ~ 100).

![](images/speaker-8-1.png)

**Example:**

![](images/speaker-8-2.png)

When Codey starts up, the sound volume of the speaker will be set to 60%.

---

## 9. volume

Report the sound volume of Codey's speaker.

![](images/speaker-9-1.png)

**Example:**

![](images/speaker-9-2.png)

Press Button A. The current sound volume of Codey's speaker will be displayed on the screen.

---

