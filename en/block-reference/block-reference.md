# Block Reference

This section includes detailed introduction of each block category.

* [Looks](looks.md)
* [Lighting](lighting.md)
* [Speaker](speaker.md)
* [Action](action.md)
* [Sensing](sensing.md)
* [Infrared](infrared.md)
* [Events](events.md)
* [Control](control.md)
* [Operators](operators.md)