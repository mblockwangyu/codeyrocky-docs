# Looks Category

**LED Dot-Matrix Display**

![](codey.jpg)

The face panel of Codey is a 128-LED dot matrix, which can display English characters, numbers, and images.

**Coordinate of the Face Panel**

![](codey-api.png)

As shown in the figure above, the face panel has the upper-left corner as the origin of the coordinate system, and the direction of x and y is indicated by the arrow. Parameters:
- x: -15 ~ 15
- y: -7 ~ 7

## 1. show image () for () secs

Display specified image on Codey's screen for a specified period of time.

![](images\1-1.png)

**Example:**

![](images\1-2.png)

Press Button A, Codey's screen will display these three images consecutively, for 1 second, 0.5 second, and 1 second respectively. After displaying all images, Codey's screen will turn off.

---

## 2. show image ()

Display specified image on Codey's screen.

![](images\2-1.png)

**Example:**

![](images\2-2.png)

When Codey starts up, the image will be displayed.

---

## 3. show image () at the x:() y:()

Display specified image at specified point of the face panel.

![](images\3-1.png)

**Example:**

![](images\3-2.png)

Press Button A, the image will be displayed at the origin.

---

## 4. turn off screen

Turn off Codey's screen.

![](images\4-1.png)

**Example:**

![](images\4-2.png)

Press Button A, the image will be displayed at the origin. One second later, the screen will turn off.

---

## 5. show ()

Display English characters, numbers and symbols on Codey's screen.

![](images\5-1.png)

**Example:**

![](images\5-2.png)

Press Button A, Codey's screen will display "12:00".

---

## 6. show () until scroll done

Display English characters, numbers and symbols on Codey's screen, and scroll all contents.

![](images\6-1.png)

**Example:**

![](images\6-2.png)

Press Button A, Codey's screen will scroll "morning".

---

## 7. show () at x:() y:()

Display English characters, numbers and symbols at specified point of the face panel.

![](images\7-1.png)

**Example:**

![](images\7-2.png)

Press Button A, Codey's screen will display "hi" at the origin.

---

## 8. light up x:() y:()

Light up specified dot (x, y) of Codey's face panel.

![](images\8-1.png)

**Parameters:**
- x: 0 ~ 15
- y: 0 ~ 7

**Example:**

![](images\8-2.png)

Press Button A, the dot at (3, 3) will light up.

---

## 9. light off x:() y:()

Light off specified dot (x, y) of Codey's face panel.

![](images\9-1.png)

**Parameters:**
- x: 0 ~ 15
- y: 0 ~ 7

**Example:**

![](images\9-2.png)

Press Button A, the dot at (2, 2) will light up, and light off after one second.

---

## 10. switch between light-up and light-off x:() y:()

Change the status of specified dot (x, y) of Codey's face panel: light off light-up dot; light up light-off dot.

![](images\10-1.png)

**Parameters:**
- x: 0 ~ 15
- y: 0 ~ 7

**Example:**

![](images\10-2.png)

Press Button A, the dot at (6, 6) will light up. Press Button A again, the dot will light off. 

---

## 11. x:() y:() is it lighted up?

If specified dot (x, y) of Codey's face panel is lighted up, the report condition is met.

![](images\11-1.png)

**Parameters:**
- x: 0 ~ 15
- y: 0 ~ 7

**Example:**

![](images\11-2.png)

Press Button A, the dot (3, 3) and (5, 5) of Codey's face panel will light up. After 3 seconds, the dot (3, 3) will light off.

---





