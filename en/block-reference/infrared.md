# Infrared Category

## 1. send IR message

Send specified IR message.

![](images/infrared-1-1.png)

**Example:**

![](images/infrared-1-2.png)

Press Button A to send IR message A.

---

## 2. IR message received

Report the received IR message.

![](images/infrared-2-1.png)

---

## 3. record home appliances remote signal 3 secs

Record the remote signal of home appliances for 3 seconds.

![](images/infrared-3-1.png)

**Example:**

![](images/infrared-3-2.png)

Press Button A to record the remote signal of home appliances for 3 seconds.

---

## 4. send home appliances remote signal

Send recorded remote signal of home appliances.

![](images/infrared-4-1.png)

**Example:**

![](images/infrared-4-2.png)

Press Button A to send recorded remote signal of home appliances.

---