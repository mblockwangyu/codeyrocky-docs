# Events

## 1. when Codey starts up

When Codey starts up, run the script.

![](images/events-1-1.png)

**Example:**

When Codey starts up, the screen will display "hello".

---

## 2. when button () is pressed

When the specified button is pressed, run the script.

![](images/events-2-1.png)

**Example:**

![](images/events-2-2.png)

When Button A is pressed, Codey will play the sound "hello".

---

## 3. when Codey is shaking

When Codey is shaking, run the script.

![](images/events-3-1.png)

**Example:**

![](images/events-3-2.png)

When being shaken, Codey will play the sound "wow".

---

## 4. when Codey is () tilted

If Codey tilts towards the specified direction, run the script. There are four options: "tilted to the left", "tilted to the right", "ears up", and "ears down".

![](images/events-4-1.png)

**Example:**

![](images/events-4-2.png)

When Codey is left-tilted, the screen will display "left".

---

## 5. when () > ()

When the value of the specified parameter (loudness or timer) is greater than the specified value, run the script.

![](images/events-5-1.png)

**Example:**

![](images/events-5-2.png)

When the loudness detected is greater than 10, Codey's screen will display current loudness.

---

## 6. when light intensity < ()

When detected light intensity is less than the specified value, run the script.

![](images/events-6-1.png)

**Example:**

![](images/events-6-2.png)

When the light intensity detected is less than 10, Codey's LED will light up red.

---

## 7. when I receive ()

When Codey receives specified message, run the script.

![](images/events-7-1.png)

**Example:**

![](images/events-7-2.png)

When receiving "message1", Codey will play the sound "laugh".

---

## 8. broadcast ()

Broadcast specified message.

![](images/events-8-1.png)

**Example:**

![](images/events-8-2.png)

Press Button A. Codey will broadcast "message 1".

---