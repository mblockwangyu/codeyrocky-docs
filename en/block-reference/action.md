# Action Category

![](rocky.png)

When Codey and Rocky are combined together, Codey Rocky can move. You can make Codey Rocky move forward or backward, turn left or right, or other more complicated movements. With the help of gyroscope, you can make Codey Rocky turn left/right a specified number of degrees, or move straight forward.

## 1. move forward at power ()% for () secs

Make Codey Rocky move forward at specified power for a specified period of time.

![](images/action-1-1.png)

**Parameters:**
- Power: -100 ~ 100 (negative number means opposite direction)
- Time: 0 ~ ∞/second

**Example:**

![](images/action-1-2.png)

When Codey starts up, Codey Rocky will move forward at 50% power for one second.

---

## 2. move backward at power ()% for () secs

Make Codey Rocky move backward at specified power for a specified period of time.

![](images/action-2-1.png)

**Parameters:**
- Power: -100 ~ 100 (negative number means opposite direction)
- Time: 0 ~ ∞/second

**Example:**

![](images/action-2-2.png)

When Codey starts up, Codey Rocky will move backward at 50% power for one second.

---

## 3. turn left at power ()% for () secs

Make Codey Rocky move turn left at specified power for a specified period of time.

![](images/action-3-1.png)

**Parameters:**
- Power: -100 ~ 100 (negative number means opposite direction)
- Time: 0 ~ ∞/second

**Example:**

![](images/action-3-2.png)

When Codey starts up, Codey Rocky will turn left at 50% power for one second.

---

## 4. turn right at power ()% for () secs

Make Codey Rocky move turn right at specified power for a specified period of time.

![](images/action-4-1.png)

**Parameters:**
- Power: -100 ~ 100 (negative number means opposite direction)
- Time: 0 ~ ∞/second

**Example:**

![](images/action-4-2.png)

When Codey starts up, Codey Rocky will turn right at 50% power for one second.

---

## 5. keep straight forward at power ()% for () secs

Use Rocky's gyroscope to make Codey Rocky move forward at the same direction at specified power for a specfied period of time.

![](images/action-5-1.png)

**Parameters:**
- Power: -100 ~ 100 (negative number means opposite direction)
- Time: 0 ~ ∞/second

**Example:**

![](images/action-5-2.png)

When Codey starts up, Codey Rocky will move straight forward at 50% power for one second.

---

## 6. keep straight backward at power ()% for () secs

Use Rocky's gyroscope to make Codey Rocky move backward at the same direction at specified power for a specfied period of time.

![](images/action-6-1.png)

**Parameters:**
- Power: -100 ~ 100 (negative number means opposite direction)
- Time: 0 ~ ∞/second

**Example:**

![](images/action-6-2.png)

When Codey starts up, Codey Rocky will move straight backward at 50% power for one second.

---

## 7. turn left () degrees until done

Make Codey Rocky turn left specified degrees and wait until it's done.

![](images/action-7-1.png)

**Parameter:**
- Angle:  -∞ ~ ∞/degree (negative number means opposite direction)

**Example:**

![](images/action-7-2.png)

When Codey starts up, Codey Rocky will turn left 15 degrees and then move forward at 50% power for one second.

---

## 8. turn right () degrees until done

Make Codey Rocky turn right specified degrees and wait until it's done.

![](images/action-8-1.png)

**Parameter:**
- Angle:  -∞ ~ ∞/degree (negative number means opposite direction)

**Example:**

![](images/action-8-2.png)

When Codey starts up, Codey Rocky will turn right 15 degrees and then move forward at 50% power for one second.

---

## 9. () at power ()%

Make Codey Rocky move forward/backward, or turn left/right at specified power.

![](images/action-9-1.png)

**Parameter:**
- Power: -100 ~ 100 (negative number means opposite direction)

**Example:**

![](images/action-9-2.png)

When Codey starts up, Codey Rocky will keep moving forward at 50% power.

---

## 10. left wheel turns at power ()% and right wheel at power ()%

Make the left wheel and right wheel of Codey Rocky move at specified power respectively.

![](images/action-10-1.png)

**Parameter:**
- Power: -100 ~ 100 (negative number means opposite direction)

**Example:**

![](images/action-10-2.png)

When Codey starts up, both the left wheel and the right wheel will move at 50% power for one second.

---

## 11. stop moving

Make Codey Rocky stop moving.

![](images/action-11-1.png)

**Example:**

![](images/action-11-2.png)

When Codey starts up, Codey Rocky will move forward at 50% power for one second, and then stop moving.

---
