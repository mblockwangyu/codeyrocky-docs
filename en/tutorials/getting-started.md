# Get Started

In this section, a quick start guide will help you get started with Codey Rocky.

## Connect Codey Rocky to mBlock 5

1\. Open mBlock 5. Connect Codey to your computer via the USB cable. (For mBlock App, please follow the on-screen instructions.)

![](2018-12-04-15-06-30.png)

2\. Press the **Power** button to turn on Codey.

![](2018-12-04-15-12-48.png)

3\. Make sure **Codey** is selected and click **Connect**. From the pop-up "Connect Device" window, click **Connect**.

![](2018-12-04-15-27-24.png)

![](2018-12-04-15-27-33.png)

## Create a Codey Rocky Project

Let's start with a simple project, to make Codey play the sound of "hello" when being shaken.

1\. Drag a <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when Codey is shaking</span> block from the Events category to the scripts area.

![](2018-12-04-15-47-37.png)

2\. Drag a <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">play sound ()</span> block from the Speaker category, and add it onto the existing block.

![](2018-12-04-15-53-06.png)

3\. Click "Upload" to upload the program to Codey.

![](2018-12-04-15-56-59.png)

4\. Try shaking Codey!