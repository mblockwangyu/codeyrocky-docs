# IR Remote

You can use your IR remote to control Codey Rocky. Customize your IR remote to be your Codey Rocky control.

## Add IR Remote Blocks

1\. Make sure "Codey" is selected and click "+" in the blocks area.

![](2018-12-05-11-20-12.png)

2\. From the pop-up Extension Center page, click "+" to add IR Remote blocks.

![](2018-12-05-17-28-07.png)

3\. Go back to Edit page. You can find IR Remote category added to the block palette.

![](2018-12-05-17-28-36.png)

## Have Some Fun

Let's try out the new IR Remote blocks. We will make a simple project to control Codey Rocky with your IR Remote.

1\. Drag a <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when Codey starts up</span> block from the Events category to the Scripts area.

![](2018-12-05-17-48-36.png)

2\. When up arrow button is pressed, we want Codey Rocky to move forward. We need to a <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">if () then ()</span> block from the Control category, a <span style="background-color:#FF6680;color:white;padding:3px; font-size: 12px">IR Remote () pressed?</span> block from the IR Remote category, and a <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">move forward at power ()% for () secs</span> block from the Action category.

![](2018-12-05-17-53-22.png)

![](2018-12-05-17-53-29.png)

![](2018-12-05-17-53-39.png)

3\. When down arrow button is pressed, we want Codey Rocky to move backward. We need to a <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">if () then ()</span> block from the Control category, a <span style="background-color:#FF6680;color:white;padding:3px; font-size: 12px">IR Remote () pressed?</span> block from the IR Remote category, and a <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">move backward at power ()% for () secs</span> block from the Action category.

![](2018-12-05-17-56-57.png)

![](2018-12-05-17-57-05.png)

4\. When left arrow button is pressed, we want Codey Rocket to turn left. We need to a <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">if () then ()</span> block from the Control category, a <span style="background-color:#FF6680;color:white;padding:3px; font-size: 12px">IR Remote () pressed?</span> block from the IR Remote category, and a <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">turn left at power ()% for () secs</span> block from the Action category.

![](2018-12-05-17-58-55.png)

![](2018-12-05-17-59-04.png)

5\. When right arrow button is pressed, we want Codey Rocky to turn right. We need to a <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">if () then ()</span> block from the Control category, a <span style="background-color:#FF6680;color:white;padding:3px; font-size: 12px">IR Remote () pressed?</span> block from the IR Remote category, and a <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">turn right at power ()% for () secs</span> block from the Action category.

![](2018-12-05-18-00-06.png)

![](2018-12-05-18-00-15.png)

6\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">forever</span>.

<img src="images/ir.png" width="800px;" style="padding:5px 5px 20px 5px;">

7\. Click "Upload" to upload the program to Codey.

![](2018-12-05-09-51-28.png)

8\. Press the buttons of your IR Remote and see how Codey Rocky moves.