# Emotion Blocks

Emotion blocks make your Codey Rocky more human-like. It can smile, get naughty, or even turn to you for care. Isn't it marvelous?

## Add Emotion Blocks

1\. Make sure "Codey" is selected and click "+" in the blocks area.

![](emotion-blocks-1.png)

2\. From the pop-up Extension Center page, click "+" to add emotion blocks.

![](emotion-blocks-2.png)

3\. Go back to Edit page. You can find emotion category added to the block palette.

![](emotion-blocks-3.png)

## Have Some Fun

Let's try out the emotion blocks. We'll make a fun game. Codey Rocky bumps into a cave full of gold coins and starts collecting them.

Before coding, you need to hang up the chocolate coins, and the toy pan.

<p style="text-align:center"><video width="320" height="240" controls><source src="emotion-blocks.mp4" type="video/mp4"></video></p>
<br/>

\> <small>Video by Jimmy Qin</small>

1\. Drag a <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when Codey starts up</span> block from the Events category to the scripts area. Add an Action block <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">move forward at power ()% for () secs</span>, with 100% power for 0.5 second.

![](emotion-blocks-4.png)

![](emotion-blocks-5.png)

2\. Add four Emotion blocks: <span style="background-color:#A3D237;color:white;padding:3px; font-size: 12px">look around</span>, <span style="background-color:#A3D237;color:white;padding:3px; font-size: 12px">wow</span>, <span style="background-color:#A3D237;color:white;padding:3px; font-size: 12px">look left</span>, and <span style="background-color:#A3D237;color:white;padding:3px; font-size: 12px">look right</span>.

![](emotion-blocks-6.png)

3\. Add another Action block <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">move forward at power ()% for () secs</span>, with 100% power for 0.5 second.

![](emotion-blocks-7.png)

4\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">repeat ()</span>, and input number 4. Then add an Emotion block <span style="background-color:#A3D237;color:white;padding:3px; font-size: 12px">yes</span> and an action block <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">move forward at power ()% for () secs</span>, with 100% power for 0.2 second.

![](emotion-blocks-8.png)

![](emotion-blocks-9.png)

![](emotion-blocks-10.png)

5\. Drag two more Emotion blocks: <span style="background-color:#A3D237;color:white;padding:3px; font-size: 12px">proud</span> and <span style="background-color:#A3D237;color:white;padding:3px; font-size: 12px">smile</span>.

![](emotion-blocks-11.png)

6\. Add another Action block <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">move forward at power ()% for () secs</span>, with 100% power for 0.6 second.

![](emotion-blocks-12.png)

7\. Last, add three more Emotion blocks: <span style="background-color:#A3D237;color:white;padding:3px; font-size: 12px">startle</span>, <span style="background-color:#A3D237;color:white;padding:3px; font-size: 12px">dizzy</span>, and <span style="background-color:#A3D237;color:white;padding:3px; font-size: 12px">hurt</span>.

![](emotion-blocks-13.png)

8\. Click "Upload" to upload the program to Codey.

![](2018-12-05-09-51-28.png)

9\. Check how Codey Rocky collect gold coins!

<a href="collect-coins.mblock" download>Click to download code</a>

