# Neuron

Take your imagination a step further with Neuron. The Neuron system includes 30 electronic blocks, from light sensor, infrared sensor, to humidity sensor. Connect your Codey to Neuron to explore more fun.

## Add Neuron Blocks

1\. Make sure "Codey" is selected and click "+" in the Blocks area.

![](2018-12-05-11-20-12.png)

2\. From the pop-up Extension Center page, click "+" to add Neuron blocks.

![](2018-12-05-11-38-06.png)

3\. Go back to Edit Page, you can find Neuron category added to the block palette.

![](2018-12-05-11-38-46.png)

## Have Some Fun

Let's try out the new Neuron Blocks. We will use the LED Strip of the Neuron system to make a simple project.

1\. Connect the LED Strip to Codey.

2\. Drag a <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when button () is pressed</span> block from the Events category to the Scripts area.

![](2018-12-05-17-04-40.png)

3\. Drag a <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">LED Strip () lights up ()</span> block from the Neuron category. Set the colors of all LEDs as repeating sequences.

![](2018-12-05-17-12-42.png)
![](2018-12-05-17-13-04.png)

4\. Click "Upload" to upload the program to Codey.

![](2018-12-05-17-13-49.png)

5\. Press button A and check how the LED strip lights up.

