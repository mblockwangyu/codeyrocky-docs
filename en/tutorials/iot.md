# IoT Blocks

The Internet of Things (IoT) connects your devices together through the internet, where data can be shared and exchanged. With built-in Wi-Fi, Codey Rcoky can connect to the internet to realize its IoT functionality, such as obtaining data of the weather. Use IoT blocks to explore cutting-edge stuff with Codey Rocky.

## Add IoT Blocks

1\. Select **Codey** and click **+ extension** in the Blocks area.

![](images/iot/iot-1.png)

2\. From the pop-up Extension Center page, click **+ Add** to add IoT blocks.

![](images/iot/iot-2.png)

3\. Go back to Edit Page, you can find the IoT category added to the Blocks area.

![](images/iot/iot-3.png)

## Have Some Fun

Let's try out the new IoT blocks. Make Codey report the weather.

1\. Drag the <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when Codey starts up</span> block from the Events category to the Scripts area.

![](images/iot/iot-4.png)

2\. Drag the <span style="background-color:#FD691D;color:white;padding:3px; font-size: 12px">connect to Wi-Fi () password ()</span> block to connect Codey to your Wi-Fi. Type in the Wi-Fi name and password.

![](images/iot/iot-5.png)

3\. Drag the <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">if () then ()</span> block from the Control category. Add a <span style="background-color:#FD691D;color:white;padding:3px; font-size: 12px">Wi-Fi connected?</span> block from the IoT category to the condition. Codey Rocky needs to join a Wi-Fi network to obtain weather data.

![](images/iot/iot-6.png)

![](images/iot/iot-7.png)

4\. Drag the <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">show ()</span> block from the Looks category, and wrap it up with the <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">if () then ()</span> block. We want Codey to show weather data, so we need to add a <span style="background-color:#FD691D;color:white;padding:3px; font-size: 12px">weather in ()</span> block from the IoT category. Type in a city name. In this example, we type in **Shenzhen** and choose it from the drop-down list.

![](images/iot/iot-8.png)

![](images/iot/iot-9.png)

5\. Click **Upload** to upload the program to Codey.

![](images/iot/iot-10.png)

6\. Check Codey's screen to see weather report.