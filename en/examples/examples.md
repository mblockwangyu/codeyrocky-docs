![](codey-rocky-product-image.jpg)

> Updated:2018/9/14
> 
> Version: mBlock v5.0.0-beta.4

# Codey Rocky Projects

## 1. Change Codey's Emotions with Buttons{# 1. Change Codey's Emotions with Buttons}

1\) Drag a <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when button () is pressed</span> block from the Events category to the Scripts area.

![](project-1-1.png)

2\) Drag a <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">play sound ()</span> block from the Speaker category.

![](project-1-2.png)

3\) Drag a <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">show image ()</span> block from the Looks category.

![](project-1-3.png)

4\) Click "Upload" to upload the program to Codey.

![](upload.png)

5\) Press Button A to see how Codey looks!

## 2. Use Codey to Create LED Animations{# 2. Use Codey to Create LED Animations}

1\) Drag a <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when Codey is shaking</span> block from the Events category to the Scripts area.

![](project-2-1.png)

2\) Drag a <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">show image () for () secs</span> block from the Looks category, and draw the following image. Reset the time to 0.3.

![](project-2-3.png)

![](project-2-2.png)

3\) Add a <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">show image () for () secs</span> block from the Looks category, and draw the following image. Reset the time to 0.3.

![](project-2-4.png)

![](project-2-5.png)

4\) Add another <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">show image () for () secs</span> block from the Looks category, and draw the following image. Reset the time to 0.3.

![](project-2-6.png)

![](project-2-7.png)

5\) Add a Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">repeat ()</span> to wrap up the three <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">show image () for () secs</span> blocks. Reset the number to 6.

![](project-2-8.png)

6\) Drag a <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">play sound ()</span> block from the Speaker category to add some sound effects. Set the sound as "jump".

![](project-2-10.png)

7\) Click "Upload" to upload the program to Codey.

![](upload.png)

8\) Try shaking Codey to see the rabbit jumping!

![](project-2-9.png)

## 3. Turn Codey into a Musician{# 3. Turn Codey into a Musician}

1\) Drag an Event block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when button () is pressed</span> to the Scripts area.

![](project-3-1.png)

2\) Add four <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">play note () for () beats</span> blocks from the Speaker category. Set the corresponding note as C4, E4, F4, and A5. 

![](project-3-2.png)

**Volume Control Script**

3\) Drag a <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when Codey starts up</span> block from the Events category.

![](project-3-3.png)

4\) Add a Speaker block <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">set volume to ()%</span> and a Sensing block <span style="background-color:#4CBFE6;color:white;padding:3px; font-size: 12px">gear potentiometer value</span>, so that we can adjust the gear to change volume.

![](project-3-4.png)

![](project-3-5.png)

5\) Add a Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">forever</span>, so we are able to adjust the sound volume all the time.

![](project-3-6.png)

6\) Click "Upload" to upload the program to Codey.

![](upload.png)

7\) Press Button A to see how Codey plays music!

## 4. Make Codey Blink Eyes{# 4. Make Codey Blink Eyes}

1\) Drag an Event block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when Codey starts up</span> to the Scripts area.

![](project-4-1.png)

2\) Add a Looks block <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">show image () for () secs</span>. Add an Operators block <span style="background-color:#59C059;color:white;padding:3px; font-size: 12px">pick random () to ()</span> to make Codey blink eyes randomly. Set the number as 2 and 5.

![](project-4-4.png)

![](project-4-2.png)

3\) Add another Looks block <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">show image () for () secs</span>. Draw the following image, and set the time as 0.2.

![](project-4-5.png)

![](project-4-3.png)

4\) Drag a Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">forever</span> to wrap up the two <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">show image () for () secs</span> blocks, so Codey will keep blinking eyes.

![](project-4-6.png)

5\) Click "Upload" to upload the program to Codey.

![](upload.png)

6\) Check Codey to see it blink eyes!

## 5. Make Codey Rocky Identify Colors

1\) Drag an Event block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when Codey starts up</span> to the Scripts area.

![](project-4-1.png)

2\) Add a Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">if () then ()</span>, and a Sensing block <span style="background-color:#4CBFE6;color:white;padding:3px; font-size: 12px">the color detected is ()?</span>.

![](project-5-1.png)

![](project-5-2.png)

3\) Add a Lighting block <span style="background-color:#B080D5;color:white;padding:3px; font-size: 12px">RGB LED lights up ()</span> to make the LED lights up red when the color detected is red. Drag a <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">play sound ()</span> block and change the sound to "score" to add some sound effects.

![](project-5-3.png)

![](project-5-4.png)

4\) We want Codey Rocky to detect color blue. Right click to duplicate the blocks. Change the color to blue.

![](project-5-5.png)

![](project-5-6.png)

5\) Add a Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">forever</span> to create a loop.

![](project-5-7.png)

6\) Click "Upload" to upload the program to Codey.

![](upload.png)

7\) Have a try!

**Note: keep the IR sensor face-down.**

![](project-5-8.png)

## 6. Make Codey Rocky Avoid Obstacles

1\) Drag an Event block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when Codey starts up</span> to the Scripts area.

![](project-4-1.png)

2\) Add a Looks block <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">show image ()</span>.

![](project-6-1.png)

3\) Drag an Action block <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">() at power ()%</span>, a Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">wait until ()</span>, and a Sensing block <span style="background-color:#4CBFE6;color:white;padding:3px; font-size: 12px">obstacles ahead?</span>. Keep all default values.

![](project-6-2.png)

![](project-6-3.png)

![](project-6-4.png)

4\) When Codey Rocky detects obstacles ahead, we want it to avoid them by moving backward and turning right, so we need to add three Action blocks: <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">stop moving</span>, <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">move backward at power ()% for () secs</span>, and <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">turn right at power ()% for () secs</span>.

![](project-6-5.png)

5\) Add a Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">forever</span> to make Codey Rocky keep avoiding obstacles.

![](project-6-6.png)

6\) Click "Upload" to upload the program to Codey.

![](upload.png)

7\) Check how Codey Rocky manages to stay away from obstacles.

**Note: adjust the IR sensor to face-forward.**

## 7. Use Codey to Make Sprites Play Instruments

This project combines stage programming with Codey.

Before we start, please toggle to disable **Upload Mode**.

![](disable-upload-mode.png)

1\) Drag an Events block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when button () is pressed</span>.

![](project-7-1.png)

2\) When Button A is pressed, we want the sprite to receive the message. We need to add an Events block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">broadcast ()</span>. Click to create a new message "A".

![](project-7-2.png)

![](project-7-3.png)

3\) Add a new sprite. Under "Sprites", click "+". From the pop-up Sprite Library window, choose "Drum" and click "OK".

![](project-7-4.png)

![](project-7-5.png)

4\) Make sure "Drum" is selected. Drag an Event block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when I receive ()</span>. Add a Sound block <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">start sound ()</span> and keep the default "high tom".

![](project-7-6.png)

![](project-7-8.png)

![](project-7-7.png)

5\) Apply some animation effect to "Drum". Add a Looks block <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">switch costume to ()</span>, and set the costume as "drum-b". Add a Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">wait () seconds</span>, and change the value to 0.2. Add another Looks block <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">switch costume to ()</span>, and set the costume as "drum-a".

![](project-7-9.png)

![](project-7-10.png)

![](project-7-11.png)

6\) Save your project. Press Button A to see what happens!

## 8. Design a Controller for Codey Rocky

This project combines stage programming with Codey.

Before we start, please toggle to disable **Upload Mode**.

![](disable-upload-mode.png)

**Program Sprites**

1\) Under "Sprites", click "&times;" to delete the default sprite "Panda", and click "+" to add a new sprite. From the pop-up Sprites Library, choose "Arrow1" and click "OK".

![](project-8-1.png)

![](project-8-2.png)

2\) Select "Arrow1". From the Events category, drag a <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when this sprite clicked</span> block and a <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">broadcast ()</span> block. Create a new message "Right".

![](project-8-3.png)

![](project-8-4.png)

3\) Right click "Arrow1" to duplicate. The new sprite will automatically be named "Arrow2". Click "Costumes" to change the costume. Select costume 2. When done, click "&times;" finish costume setting. In the Scripts area, click to create a new message "Left".

![](project-8-5.png)

![](project-8-6.png)

![](project-8-7.png)

![](project-8-8.png)

4\) Repeat step 3 to add another sprite "Arrow3" to broadcast message "Down".

![](project-8-10.png)

![](project-8-11.png)

5\) Repeat step 3 to add another sprite "Arrow4" to broadcast message "Up".

![](project-8-12.png)

![](project-8-13.png)

6\) Click to reposition the four arrows as the following image.

![](project-8-14.png)

**Program Codey**

7\) Choose "Codey" under "Devices". Drag an Events block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when I receive ()</span> and set the message as "Left". Add an Action block <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">turn left at power ()% for () secs</span>, and keep the default value.

![](project-8-15.png)

![](project-8-16.png)

8\) Drag an Events block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when I receive ()</span> and set the message to "Right". Add an Action block <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">turn right at power ()% for () secs</span>, and keep the default value.

![](project-8-17.png)

9\) Drag an Events block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when I receive ()</span> and set the message to "Up". Add an Action block <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">move forward at power ()% for () secs</span>, and keep the default value.

![](project-8-18.png)

10\) Drag an Events block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when I receive ()</span> and set the message to "Down". Add an Action block <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">move backward at power ()% for () secs</span>, and keep the default value.

![](project-8-19.png)

11\) Save your project. Click the arrows on the stage and see how Codey Rocky moves!

## 9. Make a Number Bomb Game

1\) Drag an Event block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when Codey starts up</span> to the Scripts area.

![](project-4-1.png)

2\) Choose Variables category and click "Make a Variable". Name the variable "Number" and click "OK".

![](project-9-1.png)

![](project-9-2.png)

3\) Add a Variables block <span style="background-color:#FF8C1A;color:white;padding:3px; font-size: 12px">set Number to ()</span>, and keep default value.

![](project-9-3.png)

4\) Add a Looks block <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">show () until scroll done</span> and a Variables block <span style="background-color:#FF8C1A;color:white;padding:3px; font-size: 12px">Number</span>.

![](project-9-4.png)

![](project-9-5.png)

5\) Add an Events block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when button () is pressed</span>, and keep the default value. Add a Variables block <span style="background-color:#FF8C1A;color:white;padding:3px; font-size: 12px">change Number by ()</span> and also keep the default value.

![](project-9-6.png)

![](project-9-7.png)

6\) Add a Speaker block <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">play sound ()</span> to apply some sound effects to the game.

![](project-9-8.png)

7\) Click the following two blocks to duplicate and add them to the <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">play sound ()</span> block.

![](project-9-9.png)

![](project-9-10.png)

8\) Add a Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">if () then ()</span> to set how the game ends.

![](project-9-11.png)

9\) From the Operators category, drag a <span style="background-color:#59C059;color:white;padding:3px; font-size: 12px">() &gt; ()</span> block and a <span style="background-color:#59C059;color:white;padding:3px; font-size: 12px">pick random () to ()</span> block. Set the number to 10 and 20. Then add a Variables block <span style="background-color:#FF8C1A;color:white;padding:3px; font-size: 12px">Number</span>.

![](project-9-12.png)

![](project-9-13.png)

10\) Add a Looks block <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">show image ()</span> and draw the following image. Add a Speaker block <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">play sound ()</span>, and set the sound to "explosion".

![](project-9-14.png)

![](project-9-15.png)

![](project-9-16.png)

11\) Click "Upload" to upload the program to Codey.

![](upload.png)

12\) Have a try!

# More Resources

## Example Programs

You can find a variety of built-in example programs in mBlock 5. Go to the Project Management page to access those programs.

![](examples-1.png)

## Makeblock Education

Explore more learning resources at Makeblock Education: [http://education.makeblock.com/](http://education.makeblock.com/)

## Forum

Want to share projects with other users and discuss with them? Visit Codey Rocky forum: [https://forum.makeblock.com/c/makeblock-products/codey-rocky](https://forum.makeblock.com/c/makeblock-products/codey-rocky)