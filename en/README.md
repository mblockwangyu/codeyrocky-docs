# Codey Rocky Docs

\* If you have any technical problems, please contact: <support@makeblock.com>

Thank you for your interest in Codey Rocky!

The help documents include the following parts.

* [Tutorials](README.md)
    * [Introduction](tutorials/introduction.md)
    * [Get Started](tutorials/getting-started.md)
    * [Upload Mode](tutorials/upload-mode.md)
    * [Emotion Blocks](tutorials/emotion-blocks.md)
    * [IoT](tutorials/iot.md)
    * [Neuron](tutorials/neuron.md)
    * [IR Remote](tutorials/ir.md)
    * [Use Python](tutorials/use-python.md)
* [Examples](examples/examples.md)
* [Block Reference](block-reference/block-reference.md)
* [Python API Reference](python-api/python-api.md)
* [FAQ](faq/faq.md)