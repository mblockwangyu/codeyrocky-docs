# Python API文档

本章介绍了程小奔的Python接口，一共包含以下几类模块：

- [小程的Python接口](codey-api.md)：主要是小程板载驱动的一些API接口；
- [小奔的Python接口](rocky-api.md)：小程用于控制小奔运动或者获取小奔传感器数据的一些API接口；
- [第三方类库的Python接口](third-party-api.md)：程小奔内置的一些第三方类库，例如`mqtt`，`urequest`的类；
- [神经元扩展模块Python接口](neuron-api.md)：程小奔使用神经元模块时的一些API接口。



