# 小奔的Python接口API

## `motion` --- 小奔底盘移动

**功能相关函数**

`rocky.stop()`<br/>
小奔停止运动。

`rocky.forward(speed, t = None, straight = False)`<br/>
小奔向前运动，参数：
- *speed* 运动速度的数值，参数范围 `-100 ~ 100`，负数代表后退，正数代表前进。
- *t* 运动时间的数值，单位为 `秒`，参数范围 `0 ~ 数值范围极限`，如果设置为 1，代表小车往前运动 1s就会停下来。 如果不设置该参数，在没有停止运动或者新的运动指令之前，维持前进状态。
- *straight* 是否使能陀螺仪对行进方向的校正，如果不设置该参数，则默认不启用。

`rocky.backward(speed, t = None, straigh = False)`<br/>
小奔向后运动，参数：
- *speed* 运动速度的数值，参数范围 `-100 ~ 100`，负数代表前进，正数代表后退。
- *t* 运动时间的数值，单位为 `秒`，参数范围 `0 ~ 数值范围极限`，如果设置为 1，代表小车往后运动 1s就会停下来。 如果不设置该参数，在没有停止运动或者新的运动指令之前，维持后退状态。
- *straight* 是否使能陀螺仪对行进方向的校正，如果不设置该参数，则默认不启用。

`rocky.turn_left(speed, t = None)`<br/>
小奔向左转，参数：
- *speed* 转向快慢程度的数值，参数范围 `-100 ~ 100`，负数代表右转，正数代表左转。
- *t* 运动时间的数值，单位为 `秒`，参数范围 `0 ~ 数值范围极限`，如果设置为 1，代表小车左转 1s就会停下来。 如果不设置该参数，在没有停止运动或者新的运动指令之前，维持左转状态。

`rocky.turn_right(speed, t = None)`<br/>
小奔向右转，参数：
- *speed* 转向快慢程度的数值，参数范围 `-100 ~ 100`，负数代表左转，正数代表右转。
- *t* 运动时间的数值，单位为 `秒`，参数范围 `0 ~ 数值范围极限`，如果设置为 1，代表小车右转 1s就会停下来。 如果不设置该参数，在没有停止运动或者新的运动指令之前，维持右转状态。

`rocky.drive(left_power, right_power)`<br/>
小奔电机按照设定的数值转动，参数：
- *left_power* 左轮电机的运动速度，参数范围 `-100 ~ 100`，负数代表左轮向后转动，正数代表左轮向前转动。
- *right_power* 右轮电机的运动速度，参数范围 `-100 ~ 100`，负数代表右轮向后转动，正数代表右轮向前转动。

`rocky.turn_right_by_degree(angle, speed = 40)`<br/>
小奔按照设定的角度值右转，参数：
- *angle* 代表转动的角度，负数代表左转，正数代表右转。
- *speed* 在何种速度下进行转弯，参数范围 `0 ~ 100`；如果不设置该参数，速度默认为40。（由于拐弯使用陀螺仪作为传感器，建议不要修改速度，避免转弯角度不准）

`rocky.turn_left_by_degree(angle, speed = 40)`<br/>
小奔按照设定的角度值左转，参数：
- *angle* 代表转动的角度，负数代表右转，正数代表左转。
- *speed* 在何种速度下进行转弯，参数范围 `0 ~ 100`；如果不设置该参数，速度默认为40。（由于拐弯使用陀螺仪作为传感器，建议不要修改速度，避免转弯角度不准）

**程序示例**

```py
import codey
import rocky
import time

rocky.forward(50, 1)
rocky.stop()
rocky.backward(50, 1)
rocky.turn_left(50, 1)
rocky.turn_right(50, 1)
rocky.drive(50, 80)
time.sleep(2)
while True:
    rocky.turn_right_by_degree(80, 40)
    rocky.turn_right_by_degree(80, 20)
```

## `color_ir_sensor` --- 颜色红外传感器

**颜色红外传感器说明**

![](rocky-api.png)

如图所示，小奔前方的传感器分别为：
- **白光 LED**：发出白光，配合可见光传感器可以探测物体表面的可见光反射强度。
- **可见光光线传感器**：探测可见光的强度。
- **RGB LED**：按照设定RGB数值发出光线，配合可见光传感器识别颜色。
- **红外光光线传感器**：探测红外光线的强度。
- **红外发射器**：发射红外光线，配合红外光光线传感器可以探测物体表面的红外光反射强度。

**功能相关函数**

`color_ir_sensor.get_red()`<br/>
获取颜色传感器的红色色值分量的大小，参数范围是 `0 ~ 100`。

`color_ir_sensor.get_green()`<br/>
获取颜色传感器的绿色色值分量的大小，参数范围是 `0 ~ 100`。

`color_ir_sensor.get_blue()`<br/>
获取颜色传感器的蓝色色值分量的大小，参数范围是 `0 ~ 100`。

`color_ir_sensor.is_color(color_str)`<br/>
判断是否检测到匹配的颜色，参数：
- *color_str* 颜色类型，包括 `红`、`绿`、`蓝`、`黄`、`青`、`紫`、`白`、`黑`，对应的参数是 `red`、`green`、`blue`、`yellow`、`cyan`、`purple`、`white`、`black`，返回数值为布尔值，`Ture` 为颜色匹配，`False` 为颜色不匹配。

`color_ir_sensor.get_light_strength()`<br/>
获取可见光传感器的检测到的环境光强度，数值范围 `0 ~ 100`。

`color_ir_sensor.get_greyness()`<br/>
获取可见光传感器的检测到的灰度值（使用RGB和可见光传感器），数值范围 `0 ~ 100`。

`color_ir_sensor.get_reflected_light()`<br/>
获取可见光传感器的检测到的可见光反射强度（使用白灯和可见光传感器），数值范围 `0 ~ 100`。

`color_ir_sensor.get_reflected_infrared()`<br/>
获取红外光接收管检测到的红外光反射强度，数值范围 `0 ~ 100`。

`color_ir_sensor.is_obstacle_ahead()`<br/>
检测前方是否有障碍物，返回值为布尔值，`Ture` 为有障碍物，`False` 为没有障碍物。

`color_ir_sensor.set_led_color(color_name)`<br/>
设置颜色传感器中 RGB LED灯的颜色： 参数：
- *color_name* 包括 `红`、`绿`、`蓝`、`黄`、`青`、`紫`、`白`、`黑`，对应的参数是 `red`、`green`、`blue`、`yellow`、`cyan`、`purple`、`white`、`black`。

**程序示例**

```py
import codey
import rocky

while True:
    if rocky.color_ir_sensor.is_obstacle_ahead():
        rocky.color_ir_sensor.set_led_color('white')
    else:
      rocky.color_ir_sensor.set_led_color('black')
```