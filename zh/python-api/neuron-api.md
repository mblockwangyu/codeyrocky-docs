# 神经元扩展模块Python接口

## `dc_motor_driver` --- 双直流电机驱动模块

**功能相关函数**

`dc_motor_driver.set_power(speed, ch = 0)`<br/>
设置双路直流电机各路电机的动力，参数：
- *speed* 控制目标电机的动力值，参数范围是 `-100 ~ 100`。
- *ch* 控制的电机通道，参数范围是 `0 ~ 2`，其中 `0` 表示两路电机通道，`1` 表示插槽1通道，`2` 表示插槽2通道。

**程序示例**

```py
import codey
import neurons
import event

@event.button_a_pressed
def on_button_a_pressed():
    print("button a event succeeded")
    neurons.dc_motor_driver.set_power(100, 1)

@event.button_b_pressed
def on_button_b_pressed():
    print("button b event succeeded")
    neurons.dc_motor_driver.set_power(100, 2)

@event.button_c_pressed
def on_button_c_pressed():
    print("button c event succeeded")
    neurons.dc_motor_driver.set_power(100, 0)
    neurons.dc_motor_driver.set_power(100, 1, 2)
```

## `servo_driver` --- 双舵机驱动模块

**功能相关函数**

`servo_driver.set_angle(position, ch = 0)`<br/>
设置双路舵机各路电机的动力，参数：
- *position* 控制目标舵机的转动角度，参数范围是 `0 ~ 180`。
- *ch* 控制的舵机通道，参数范围是 `0 ~ 2`，其中 `0` 表示两路舵机通道，`1` 表示插槽1通道，`2` 表示插槽2通道。

**程序示例**

```py
import codey
import neurons
import event
import time

neurons.servo_driver.set_angle(0, 0)
time.sleep(1)

@event.button_a_pressed
def on_button_a_pressed():
    print("button a event succeeded")
    neurons.servo_driver.set_angle(100, 1)

@event.button_b_pressed
def on_button_b_pressed():
    print("button b event succeeded")
    neurons.servo_driver.set_angle(100, 2)

@event.button_c_pressed
def on_button_c_pressed():
    print("button c event succeeded")
    neurons.servo_driver.set_angle(100, 0)
```

## `led_strip` --- 灯带驱动模块

**功能相关函数**

`led_strip.set_single(index, red_value, green_value, blue_value)`<br/>
设置灯带上单个灯的颜色, 参数：
- *index* 设置第几个灯，参数范围是 `1 ~ 灯带的最大灯珠数目`。
- *red_value* 全彩LED红色分量的数值，参数范围是 `0 ~ 255`， 0为无红色分量，255是红色分量最亮。
- *green_value* 全彩LED绿色分量的数值，参数范围是 `0 ~ 255`， 0为无绿色分量，255是绿色分量最亮。
- *blue_value* 全彩LED蓝色分量的数值，参数范围是 `0 ~ 255`， 0为无蓝色分量，255是蓝色分量最亮。

`led_strip.set_all(red_value, green_value, blue_value)`<br/>
设置全部灯带的颜色, 参数：
- *red_value* 全彩LED红色分量的数值，参数范围是 `0 ~ 255`， 0为无红色分量，255是红色分量最亮。
- *green_value* 全彩LED绿色分量的数值，参数范围是 `0 ~ 255`， 0为无绿色分量，255是绿色分量最亮。
- *blue_value* 全彩LED蓝色分量的数值，参数范围是 `0 ~ 255`， 0为无蓝色分量，255是蓝色分量最亮。

`led_strip.set_effect(mode, speed, list)`<br/>
设置灯条的灯效，参数：
- *mode* 灯效的模式，参数范围是 0 ~ 5，其中：
    * 0： 表示静态模式，按设定的颜色点亮前N个灯，未设置的灯珠处于熄灭状态。
    * 1： 表示滚动模式，按设定的颜色点亮前N个灯，按设定动态变化速度，N个灯图案，向后滚动，每次滚动一个灯，循环滚动。如下图：<br/>
    ![](neuron-api-1.png)
    * 2： 表示重复模式，按设定的颜色点亮前N个灯，后面的灯重复前面N个灯的效果，直到最后一个灯。如下图：<br/>
    ![](neuron-api-2.png)
    * 3： 表示跑马灯模式，重复的效果加上动态变化，按设定的动态变化速度，N个灯循环挪动。如下图：<br/>
    ![](neuron-api-3.png)
    * 4： 表示呼吸灯模式，呼吸灯的变化速度按人的呼吸频率3秒1次。
    * 5： 表示渐变模式，整条灯带从原来的颜色渐变到新设定的颜色，变化间隔时间可设定。
- *speed* 动态变化速度，参数范围是 `0 ~ 8`，0最慢，8最快。(只针对有动态变化的灯效的设置)。
- *list* 可变参数列表，每个数值的参数范围是 `0 ~ 8`，第1个参数表示第一个灯的颜色，第2个参数表示第二个灯的颜色...，颜色的参数如下： `黑(0x00)`，`红(0x01)`，`橙(0x02)`，`黄(0x03)`，`绿(0x04)`，`青(0x05)`，`蓝(0x06)`，`紫(0x07)`，`白(0x08)`。

**程序示例**

```py
import codey
import neurons
import event
import time

neurons.led_strip.set_all(0, 0, 255)
time.sleep(1)

@event.button_a_pressed
def on_button_a_pressed():
    print("button a event successed")
    neurons.led_strip.set_all(0, 0, 0)
    neurons.led_strip.set_single(1, 255, 0, 0)
    time.sleep(1)
    neurons.led_strip.set_all(0, 0, 0)
    neurons.led_strip.set_single(2, 255, 0, 0)
    time.sleep(1)
    neurons.led_strip.set_all(0, 0, 0)
    neurons.led_strip.set_single(3, 255, 0, 0)
    time.sleep(1)

@event.button_b_pressed
def on_button_b_pressed():
    print("button b event successed")
    neurons.led_strip.set_effect(0, 8, (1,6,8,1,6,8,1,6,8))
    time.sleep(3)
    neurons.led_strip.set_effect(1, 8, (1,6,8,1,6,8,1,6,8))
    time.sleep(3)
    neurons.led_strip.set_effect(2, 8, (1,6,8,1,6,8,1,6,8))
    time.sleep(3)
    neurons.led_strip.set_effect(3, 8, (1,6,8,1,6,8,1,6,8))
    time.sleep(3)
    neurons.led_strip.set_effect(4, 8, (1,6,8,1,6,8,1,6,8))
    time.sleep(3)
    neurons.led_strip.set_effect(5, 8, (1,6,8,1,6,8,1,6,8))
    time.sleep(3)

@event.button_c_pressed
def on_button_c_pressed():
    print("button c event successed")
    neurons.led_strip.set_effect(0, 5, (1,1,1,1,1,1,1,1,1))
    time.sleep(3)
    neurons.led_strip.set_effect(1, 5, (1,1,1,1,1,1,1,1,1))
    time.sleep(3)
    neurons.led_strip.set_effect(2, 5, (1,1,1,1,1,1,1,1,1))
    time.sleep(3)
    neurons.led_strip.set_effect(3, 5, (1,1,1,1,1,1,1,1,1))
    time.sleep(3)
    neurons.led_strip.set_effect(4, 5, (1,1,1,1,1,1,1,1,1))
    time.sleep(3)
    neurons.led_strip.set_effect(5, 5, (1,1,1,1,1,1,1,1,1))
    time.sleep(3)
```

## `led_panel` --- LED面板模块

**功能相关函数**

`led_panel.set_all(red_value, green_value, blue_value)`<br/>
设置并显示整个LED面板的颜色, 参数：
- *red_value* 全彩LED红色分量的数值，参数范围是 `0 ~ 255`， 0为无红色分量，255是红色分量最亮。
- *green_value* 全彩LED绿色分量的数值，参数范围是 `0 ~ 255`， 0为无绿色分量，255是绿色分量最亮。
- *blue_value* 全彩LED蓝色分量的数值，参数范围是 `0 ~ 255`， 0为无蓝色分量，255是蓝色分量最亮。

`led_panel.set_pixel(x, y, red_value, green_value, blue_value)`<br/>
设置LED面板单个像素点的颜色，参数：
- *x* 像素点在表情面板上x轴的坐标，参数范围是 `0 ~ 7`。
- *y* 像素点在表情面板上y轴的坐标，参数范围是 `0 ~ 7`。
- *red_value* 全彩LED红色分量的数值，参数范围是 `0 ~ 255`， 0为无红色分量，255是红色分量最亮。
- *green_value* 全彩LED绿色分量的数值，参数范围是 `0 ~ 255`， 0为无绿色分量，255是绿色分量最亮。
- *blue_value* 全彩LED蓝色分量的数值，参数范围是 `0 ~ 255`， 0为无蓝色分量，255是蓝色分量最亮。

`led_panel.show_image(list, mode = 0)`<br/>
以图片参数的方式设置显示的内容，参数：
- *list* 可变参数列表，每个数值的参数范围是 `0 ~ 8`，第1个参数表示第一个灯的颜色，第2个参数表示第二个灯的颜色...，颜色的参数如下： `黑(0x00)`，`红(0x01)`，`橙(0x02)`，`黄(0x03)`，`绿(0x04)`，`青(0x05)`，`蓝(0x06)`，`紫(0x07)`，`白(0x08)`。
- *mode* 显示内容的呈现方式，参数范围是 `0 ~ 3`，其中：
    * 0：表示显现模式，直接将设定的图案显示出来。
    * 1：表示擦除模式，原图像按竖列逐渐消失，同时设定的图像按竖列逐渐显现出来。
    * 2：表示左移模式，原图像向左移动消失，设定的图像向左移动显现出来
    * 3：表示右移模式，原图像向右移动消失，设定的图像向右移动显现出来

`led_panel.set_animation(frame_index, list)`<br/>
设置LED面板的动画帧内容，参数：
- *frame_index* 动画帧的帧序列，参数范围是 `0 ~ 3`，0表示第一帧。
- *list* 可变参数列表，每个数值的参数范围是 `0 ~ 8`，第1个参数表示第一个灯的颜色，第2个参数表示第二个灯的颜色...，颜色的参数如下： `黑(0x00)`，`红(0x01)`，`橙(0x02)`，`黄(0x03)`，`绿(0x04)`，`青(0x05)`，`蓝(0x06)`，`紫(0x07)`，`白(0x08)`。

`led_panel.show_animation(frame_speed, mode)`<br/>
显示 set_animation 设定的动画帧内容，参数：
- *frame_speed* 动画帧的切换速度，参数范围是 `0 ~ 2`，其中：
    * 0：表示慢速，1秒的滚动间隔。
    * 1：表示正常，0.5秒的滚动间隔。
    * 2：表示快速，0.2秒的滚动间隔。
- *mode* 帧变化的模式，参数范围是 `0 ~ 3`，其中：
    * 0：表示显现模式，直接将设定的图案显示出来。
    * 1：表示擦除模式，原图像按竖列逐渐消失，同时设定的图像按竖列逐渐显现出来。
    * 2：表示左移模式，原图像向左移动消失，设定的图像向左移动显现出来
    * 3：表示右移模式，原图像向右移动消失，设定的图像向右移动显现出来

`led_panel.show_string(red_value, green_value, blue_value, list)`<br/>
按指定颜色显示字符串，参数：
- *red_value* 全彩LED红色分量的数值，参数范围是 `0 ~ 255`， 0为无红色分量，255是红色分量最亮。
- *green_value* 全彩LED绿色分量的数值，参数范围是 `0 ~ 255`， 0为无绿色分量，255是绿色分量最亮。
- *blue_value* 全彩LED蓝色分量的数值，参数范围是 `0 ~ 255`， 0为无蓝色分量，255是蓝色分量最亮。
- *list* 可变参数列表，第1个字符，第2个字符...

`led_panel.clear()`<br/>
清除面板的显示，即所有LED灯珠都熄灭。

**程序示例**

```py
import codey
import neurons
import event
import time

neurons.led_panel.clear()
neurons.led_panel.set_all(0, 0, 255)
time.sleep(1)
neurons.led_panel.clear()

@event.button_a_pressed
def on_button_a_pressed():
    print("button a event successed")
    neurons.led_panel.set_pixel(0, 0, 255, 0, 0)
    time.sleep(1)
    neurons.led_panel.set_pixel(4, 4, 255, 0, 0)
    time.sleep(1)
    neurons.led_panel.set_pixel(7, 7, 255, 0, 0)
    time.sleep(1)
    neurons.led_panel.set_pixel(0, 6, 255, 0, 0)
    time.sleep(1)

@event.button_b_pressed
def on_button_b_pressed():
    print("button b event successed")
    neurons.led_panel.show_image([1,6,8,0,0,0,1,6,8],0)
    time.sleep(1)
    neurons.led_panel.show_image([1,1,1,1,1,1,1,1,1],1)
    time.sleep(1)
    neurons.led_panel.show_image([6,6,6,6,6,6,6,6,6],2)
    time.sleep(1)
    neurons.led_panel.show_image([8,8,8,8,8,8,8,8,8],3)
    time.sleep(1)

@event.button_c_pressed
def on_button_c_pressed():
    print("button c event successed")
    neurons.led_panel.set_animation(0, (1,6,8,1,6,8,0,0,0))
    neurons.led_panel.set_animation(1, (6,6,6,6,6,6,6,6,6))
    neurons.led_panel.set_animation(2, [6,6,6,6,6,6,6,6,6])
    neurons.led_panel.set_animation(3, (8,8,8,8,8,8,8,8,8))
    neurons.led_panel.show_animation(1, 2)
    time.sleep(6)
    neurons.led_panel.show_string(255, 0, 0, "hello")
    time.sleep(4)
    neurons.led_panel.show_string(255, 0, 0, (100))
    time.sleep(4)
    neurons.led_panel.show_string(255, 0, 0, (1,2,3))
    time.sleep(4)
```

## `buzzer` --- 蜂鸣器模块

**功能相关函数**

`buzzer.play_note(note_num, beat = None)`<br/>
播放音符， 数字音符定义请参考： [数字音符说明](https://en.scratch-wiki.info/wiki/Play_Note_()_for_()_Beats_(block))，参数：
- *note_num* 数值型，数值范围 `48 - 72`，或者字符串类型，如 C4。
- *beat* 数值数据，表示节拍数，如果不填，则一直播放。
音符与频率的对应关系如下:<br/>
```
['C2','65'],   ['D2','73'],   ['E2','82'],   ['F2','87'],
['G2','98'],   ['A2','110'],  ['B2','123'],  ['C3','131'],
['D3','147'],  ['E3','165'],  ['F3','175'],  ['G3','196'],
['A3','220'],  ['B3','247'],  ['C4','262'],  ['D4','294'],
['E4','330'],  ['F4','349'],  ['G4','392'],  ['A4','440'],
['B4','494'],  ['C5','523'],  ['D5','587'],  ['E5','659'],
['F5','698'],  ['G5','784'],  ['A5','880'],  ['B5','988'],
['C6','1047'], ['D6','1175'], ['E6','1319'], ['F6','1397'],
['G6','1568'], ['A6','1760'], ['B6','1976'], ['C7','2093'],
['D7','2349'], ['E7','2637'], ['F7','2794'], ['G7','3136'],
['A7','3520'], ['B7','3951'], ['C8','4186'], ['D8','4699'],
```

`buzzer.play_tone(frequency, time = None)`<br/>
播放设定频率的声音，参数：
- *frequency* 数值数据，播放声音的频率，其数值范围是 `0 ~ 5000`。
- *time* 数值数据，表示播放时间(单位是 `毫秒-ms` )，其数值范围是 `0 ~ 数值范围极限`。

`buzzer.rest(number)`<br/>
停止节拍，参数：
- *number* 数值数据，暂停的节拍数，其数值范围是 `0 ~ 数值范围极限`。

**常量**

`buzzer.tempo`<br/>
数值数据，数值范围是 `6 ~ 600`，表示播放速度的属性，单位是 `bmp(beat per minute)`，即每一个节拍的长度。 默认数值是60，即一个节拍的维持时间是1秒。 `rest` 和 `play_note` 函数的节拍会受该常量影响。

**程序示例**

```py
import codey
import time
import neurons

codey.display.show("hello")

neurons.buzzer.play_note(48, 1)
neurons.buzzer.rest(1)
codey.display.show("note")
codey.display.clear()
neurons.buzzer.play_note("C4", 1)
neurons.buzzer.rest(1)
codey.display.show("C4")
codey.display.clear()
neurons.buzzer.play_tone(1000, 2)
neurons.buzzer.rest(1)
codey.display.show("tone")
codey.display.clear()

while True:
    neurons.buzzer.tempo = 60
    print("tempo:", end = "")
    print(neurons.buzzer.tempo)
    neurons.buzzer.play_note("C4", 1)
    neurons.buzzer.rest(2)
    neurons.buzzer.tempo = 240
    neurons.buzzer.play_note("C4", 1)
    neurons.buzzer.rest(2)
```

## `button` --- 按钮模块

**功能相关函数**

`button.is_pressed()`<br/>
获取按键的当前状态。 返回的结果是 `True`：按键被按下， 或者 `False`: 按键未被按下。

**程序示例**

```py
import codey
import neurons

while True:
    if neurons.button.is_pressed():
        print("pressed!")
```

## `funny_touch` --- 触摸开关（四控）模块

**触摸开关（四控）使用说明**

![](neuron-api-4.png)

触摸开关可以连接导电的物品（如香蕉、水），将它变成触摸开关。通过检测四色鳄鱼夹和地线的导通状态，实现简单有趣的交互效果。

如何使用：

1. 将四色鳄鱼夹插到插槽 1，地线插到插槽 2。
2. 用鳄鱼夹夹住一个导电物体。
3. 抓住地线的金属夹子，同时用另一只手触摸导电物体，触摸开关相应指示灯亮起，模块将发出一个触发信号。

注：鳄鱼夹比较锋利，请不要用四色鳄鱼夹或地线夹子夹自己或他人，否则可能会造成伤害

**功能相关函数**

`funny_touch.is_red_touched()`<br/>
四色鳄鱼夹的红色鳄鱼夹是否有被触摸（或者间接触摸），返回的结果是 `True`：被触摸了， 或者 `False`: 未被触摸。

`funny_touch.is_green_touched()`<br/>
四色鳄鱼夹的绿色鳄鱼夹是否有被触摸（或者间接触摸），返回的结果是 `True`：被触摸了， 或者 `False`: 未被触摸。

`funny_touch.is_yellow_touched()`<br/>
四色鳄鱼夹的黄色鳄鱼夹是否有被触摸（或者间接触摸），返回的结果是 `True`：被触摸了， 或者 `False`: 未被触摸。

`funny_touch.is_blue_touched()`<br/>
四色鳄鱼夹的蓝色鳄鱼夹是否有被触摸（或者间接触摸），返回的结果是 `True`：被触摸了， 或者 `False`: 未被触摸。

**程序示例**

```py
import codey
import time
import event
import neurons

@event.start
def start_cb():
    while True:
        if neurons.funny_touch.is_blue_touched():
            print("blue touched")
        if neurons.funny_touch.is_red_touched():
            print("red touched")
        if neurons.funny_touch.is_green_touched():
            print("green touched")
        if neurons.funny_touch.is_yellow_touched():
            print("yellow touched")

        time.sleep(0.1)
```

## `ultrasonic_sensor` --- 超声波传感器模块

**功能相关函数**

`ultrasonic_sensor.get_distance()`<br/>
获取超声波传感器测量的前方障碍物的距离，单位是 `厘米`，返回的数据是浮点类型数值。 测量的范围是 `3 ~ 300` 厘米，3厘米以内的测量数据会不准确，数值的范围是 `0 ~ 300` 厘米。

**程序示例**

```py
import codey
import time
import event
import neurons

@event.start
def start_cb():
    while True:
        print(neurons.ultrasonic_sensor.get_distance())
        time.sleep(0.2)
```

## `gyro_sensor` --- 陀螺仪传感器

**陀螺仪传感器说明**

神经元模块的陀螺仪的坐标体系如下图所示：<br/>
![](neuron-api-5.png)

**功能相关函数**

`gyro_sensor.get_roll()`<br/>
获取姿态角的翻滚角，返回的数据范围是 `-90 ~ 90`

`gyro_sensor.get_pitch()`<br/>
获取姿态角的俯仰角，返回的数据范围是 `-180 ~ 180`

`gyro_sensor.get_yaw()`<br/>
获取姿态角的偏航角，返回的数据范围是 `-32768 ~ 32767`，由于板载的陀螺仪模块是六轴传感器，没有电子罗盘。 所以实际上偏航角只是使用了Z轴角速度的积分。它存在着积累误差。如果是想获得真实偏航角的，这个API不适合使用。

`gyro_sensor.is_shaked()`<br/>
检测神经元的陀螺仪模块是否有被摇晃，返回值是布尔值，其中 `True` 表示陀螺仪模块被晃动了，`False` 表示陀螺仪模块未被晃动。

`gyro_sensor.get_acceleration(axis)`<br/>
获取三个轴的加速度值，单位是 `g`，参数：
- *axis* 字符串类型，以 `x`，`y`，`z` 代表陀螺仪模块定义的坐标轴。

`gyro_sensor.get_gyroscope(axis)`<br/>
获取三个轴的角速度值，单位是 `°/秒`，参数：
- *axis* 字符串类型，以 `x`，`y`，`z` 代表陀螺仪模块定义的坐标轴。

**程序示例1：**

```py
import rocky
import event
import neurons

@event.button_a_pressed
def on_button_a_callback():
    codey.stop_other_scripts()
    codey.display.show("pit")
    while True:
        print(neurons.gyro_sensor.get_pitch())
        time.sleep(0.05)

@event.button_b_pressed
def on_button_b_callback():
    codey.stop_other_scripts()
    codey.display.show("rol")
    while True:
        print(neurons.gyro_sensor.get_roll())
        time.sleep(0.05)

@event.button_c_pressed
def on_button_c_callback():
    codey.stop_other_scripts()
    codey.display.show("yaw")
    while True:
        print(neurons.gyro_sensor.get_yaw())
        time.sleep(0.05)
```

**程序示例2：**

```py
import rocky
import event
import neurons

@event.start
def start_cb():
    codey.display.show("sha")
    while True:
        print(neurons.gyro_sensor.is_shaked())
        time.sleep(0.2)
```

**程序示例3：**

```py
import rocky
import event
import neurons

@event.start
def start_cb():
    while True:
        print("gyro z:", end = "")
        print(neurons.gyro_sensor.get_gyroscope("z"))
        print("accel z:", end = "")
        print(neurons.gyro_sensor.get_acceleration("z"))
        time.sleep(0.2)
```

## `pir_sensor` --- 人体红外传感器模块

**功能相关函数**

`pir_sensor.is_activated()`<br/>
获取传感器的检测结果。 返回的结果是 `True`：检测到附近有人， 或者 `False`: 未检测到活动的人。

**程序示例**

```py
import codey
import time
import event
import neurons

@event.start
def start_cb():
    while True:
        print(neurons.pir_sensor.is_activated())
        time.sleep(0.2)
```

## `soil_moisture` --- 土壤湿度传感器模块

**功能相关函数**

`soil_moisture.get_value()`<br/>
获取传感器检测到的土壤湿度值，数值的范围是 `0 ~ 100` 数值越大，表示土壤湿度越高。

**程序示例**

```py
import codey
import time
import event
import neurons

@event.start
def start_cb():
    while True:
        print(neurons.soil_moisture.get_value())
        time.sleep(0.2)
```

