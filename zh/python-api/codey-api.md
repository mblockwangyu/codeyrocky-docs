# 小程的Python接口API

## `led` &mdash; 板载全彩LED灯

**功能相关函数**

`led.show(r,g,b)`<br/>
设置并显示RGB LED灯的颜色，参数：
- *r* 全彩LED红色分量的数值，参数范围是 `0 ~ 255`， 0为无红色分量，255是红色分量最亮。
- *g* 全彩LED绿色分量的数值，参数范围是 `0 ~ 255`， 0为无绿色分量，255是绿色分量最亮。
- *b* 全彩LED蓝色分量的数值，参数范围是 `0 ~ 255`， 0为无蓝色分量，255是蓝色分量最亮。

`led.set_red(val)`<br/>
设置 RGB LED灯的红色色值，参数：
- *val* 全彩LED红色分量的数值，参数范围是 `0 ~ 255`， 0为无红色分量，255是红色分量最亮。

`led.set_green(val)`<br/>
设置 RGB LED灯的绿色色值，参数：
- *val* 全彩LED绿色分量的数值，参数范围是 `0 ~ 255`， 0为无绿色分量，255是绿色分量最亮。

`led.set_blue(val)`<br/>
设置 RGB LED灯的蓝色色值，参数：
- *val* 全彩LED红色分量的数值，参数范围是 `0 ~ 255`， 0为无蓝色分量，255是蓝色分量最亮。

`led.off()`<br/>
熄灭LED灯。

**程序示例**

```py
import codey
import time

codey.led.show(255,255,255)
time.sleep(2)
codey.led.off()
time.sleep(2)
while True:
    codey.led.set_red(255)
    time.sleep(1)
    codey.led.set_green(255)
    time.sleep(1)
    codey.led.set_blue(255)
    time.sleep(1)
    codey.led.off()
    time.sleep(1)
```

## `display` &mdash; 表情面板

**表情面板显示说明**

![](codey-api.png)

如上图所示，表情面板以左上角为坐标 0 点， x ，y 的方向如箭头示意。显示图片参数时，以上图为例，其第一列数据，上面的三个是点亮的，在数据转换时，这个数据转换为 `11100000`， 即 16进制 0xe0，同样的，第二列数据转换为 `00001101`，即 16进制的 0x0d，上图的全部点阵转换为图片参数时，是 `e00d0000000000000000000000000000`。

**功能相关函数**

`display.show_image(image, pos_x = 0, pos_y = 0, time_s = None)`<br/>
以图片参数的方式显示自定义的点阵图形，参数：
- *image* 字符串数据，点阵的每一列有8个显示点，为1 byte的数据，转换为16进制的字符串, 因此16列点阵，需要用 32 个字符串数据来表示。
- *pos_x* 显示图形在表情面板上x轴的偏移量，参数范围是 `-15 ~ 15`，如果不设置该参数，默认从 0位置开始。
- *pos_y* 显示图形在表情面板上y轴的偏移量，参数范围是 `-7 ~ 7`，如果不设置该参数，默认从 0位置开始。
- *time_s* 显示暂留的时间(以秒为单位)，如果没有设置该参数，在有清屏或者重新设置表情面板操作之前，维持显示不变。

`display.show(var, pos_x = 0, pos_y = 0, wait = True)`<br/>
以全类型的数据参数方式显示数据，参数：
- *var* 全类型， 其中数值型和时间类型的显示会做特殊处理，时间格式显示需满足： `[x]x:[x]x` 格式 (正则表达式 `\d?\d:\d\d?`)。
- *pos_x* 显示数据在表情面板上x轴的偏移量，参数范围是 `-15 ~ 15`，如果不设置该参数，默认从 0位置开始。
- *pos_y* 显示数据在表情面板上y轴的偏移量，参数范围是 `-7 ~ 7`，如果不设置该参数，默认从 0位置开始。
- *wait* 设置是否阻塞显示，其中 `True`：表示阻塞直到显示完毕， `False`：表示显示但不阻塞。

`display.set_pixel(pos_x, pos_y, status)`<br/>
设置表情面板单个像素点的亮灭状态，参数：
- *pos_x* 像素点在表情面板上x轴的坐标，参数范围是 `0 ~ 15`。
- *pos_y* 像素点在表情面板上y轴的坐标，参数范围是 `0 ~ 7`。
- *status* 布尔值，其中 `True`：表示像素点亮， `False`：表示像素熄灭。

`display.get_pixel(pos_x, pos_y)`<br/>
获得表情面板上单个像素点当前的亮灭状态，返回值是布尔值，其中 `True`：表示像素点亮， `False`：表示像素熄灭，参数：
- *pos_x* 像素点在表情面板上x轴的坐标，参数范围是 `0 ~ 15`。
- *pos_y* 像素点在表情面板上y轴的坐标，参数范围是 `0 ~ 7`。

`display.toggle_pixel(pos_x, pos_y)`<br/>
切换表情面板上单个像素点当前的亮灭状态，参数：
- *pos_x* 像素点在表情面板上x轴的坐标，参数范围是 `0 ~ 15`。
- *pos_y* 像素点在表情面板上y轴的坐标，参数范围是 `0 ~ 7`。

`display.clear()`<br/>
熄灭表情面板上全部的灯。

**程序示例：**

```py
import codey
import time

codey.display.show("ffffff")
codey.display.show("123")
time.sleep(1)
codey.display.show("12345", 3, 1)
codey.display.set_pixel(1, 1, True)
image = "ffffffffff000000000000000000000000"
codey.display.show_image(image, pos_x = 3, pos_y = 4)
time.sleep(1)
codey.display.clear()
print("[1, 1]:", codey.display.get_pixel(1, 1))
codey.display.show("12:28")
while True:
    codey.display.toggle_pixel(7, 2)
    codey.display.toggle_pixel(7, 4)
    time.sleep(1)
```

## `speaker` &mdash; 板载扬声器

**功能相关函数**

`speaker.stop_sounds()`<br/>
停止所有声音。

`speaker.play_melody(file_name)`<br/>
播放音频文件，该函数播放时，不会阻塞，但连续调用的话，后一次播放操作会停止前一次的播放，参数：
- *file_name* 字符串类型，烧录在程小奔flash中的wav格式的音频文件名，输入时，也可省略格式的后缀 `.wav`。

可选择的音效文件有<br/>
```
hello.wav       ： hello（哈喽）
hi.wav          ： hi（嗨）
bye.wav         ： bye（拜）
yeah.wav        ： yeah（耶）
wow.wav         ： wow（哇哦）
laugh.wav       ： laugh（笑声）
hum.wav         ： hum（哼唱）
sad.wav         ： sad（难过）
sigh.wav        ： sigh（叹气）
annoyed.wav     ： annoyed（哼）
angry.wav       ： angry（生气）
surprised.wav   ： scared（惊吓）
yummy.wav       ： pettish（撒娇）
curious.wav     ： curious（好奇）
embarrassed.wav ： embarrassed（尴尬）
ready.wav       ： ready（准备）
sprint.wav      ： sprint（冲刺）
sleepy.wav      ： snore（打呼）
meow.wav        ： meow（喵）
start.wav       ： start（启动）
switch.wav      ： switch（开关）
beeps.wav       ： beeps（哔哔）
buzzing.wav     ： buzz（蜂鸣）
exhaust.wav     ： air-out（排气）
explosion.wav   ： explosion（爆炸）
gotcha.wav      ： gotcha（获取）
hurt.wav        ： painful（痛苦）
jump.wav        ： jump（跳动）
laser.wav       ： laser（激光）
level up.wav    ： level-up（升级）
low energy.wav  ： low-energy（低能量）
metal clash.wav ： metal-clash（金属音）
prompt tone.wav ： prompt-tone（提示）
right.wav       ： right（正确）
wrong.wav       ： wrong（错误）
ring.wav        ： ringtone（铃声）
score.wav       ： score（得分）
shot.wav        ： shot（发射）
step_1.wav      ： step_1（脚步声1）
step_2.wav      ： step_2（脚步声2）
wake.wav        ： activate（激活）
warning.wav     ： warning（警告）
```

`speaker.play_melody_until_done(file_name)`<br/>
播放音频文件直到停止，该函数会阻塞播放，即在未播放完音效之前，后一条指令无法得到执行，参数：
- *file_name* 字符串类型，烧录在程小奔flash中的wav格式的音频文件名，输入时，也可省略格式名 `.wav`，具体可选参数见 `play_melody`。

`speaker.play_note(note_num, beat = None)`<br/>
播放音符， 数字音符定义请参考： 图形化编程的数字音符说明，参数：
- *note_num* 数值型，数值范围 `48 - 72`，或者字符串类型，如 `C4`。
- *beat* 数值数据，表示节拍数，如果不填，则一直播放。

音符与频率的对应关系如下:<br/>
```
['C2','65'],   ['D2','73'],   ['E2','82'],   ['F2','87'],
['G2','98'],   ['A2','110'],  ['B2','123'],  ['C3','131'],
['D3','147'],  ['E3','165'],  ['F3','175'],  ['G3','196'],
['A3','220'],  ['B3','247'],  ['C4','262'],  ['D4','294'],
['E4','330'],  ['F4','349'],  ['G4','392'],  ['A4','440'],
['B4','494'],  ['C5','523'],  ['D5','587'],  ['E5','659'],
['F5','698'],  ['G5','784'],  ['A5','880'],  ['B5','988'],
['C6','1047'], ['D6','1175'], ['E6','1319'], ['F6','1397'],
['G6','1568'], ['A6','1760'], ['B6','1976'], ['C7','2093'],
['D7','2349'], ['E7','2637'], ['F7','2794'], ['G7','3136'],
['A7','3520'], ['B7','3951'], ['C8','4186'], ['D8','4699'],
```

`speaker.play_tone(frequency, time = None)`<br/>
播放设定频率的声音，参数：
- *frequency* 数值数据，播放声音的频率，其数值范围是 `0 ~ 5000`。
- *time* 数值数据，表示播放时间(单位是 毫秒-ms )，其数值范围是 `0 ~ 数值范围极限`。

`speaker.rest(number)`<br/>
停止节拍，参数：
- *number* 数值数据，暂停的节拍数，其数值范围是 `0 ~ 数值范围极限`。

**常量**

`speaker.volume`<br/>
数值数据，音量的大小的属性值，可以修改或者读取这个值。修改这个数值，可以控制音量的大小。其数值范围是 `0 ~ 100`。

`speaker.tempo`
数值数据，表示播放速度的属性，单位是 `bmp` (beat per minute)，即每一个节拍的长度。 其数值范围是 `6 ~ 600`。 默认数值是60，即一个节拍的维持时间是1秒。 `rest` 和 `play_note` 函数的节拍会受该常量影响。

**程序示例**

```py
import codey
import time

codey.speaker.play_melody("hello", True)
codey.display.show("hello")
codey.display.clear()

codey.speaker.play_note(48, 1)
codey.speaker.rest(1)
codey.display.show("note")
codey.display.clear()
codey.speaker.play_note("C4", 1)
codey.speaker.rest(1)
codey.display.show("C4")
codey.display.clear()
codey.speaker.play_tone(1000, 2)
codey.speaker.rest(1)
codey.display.show("tone")
codey.display.clear()
print("tempo:", end = "")
print(codey.speaker.tempo)
codey.speaker.play_note("C4", 1)
codey.speaker.rest(1)
codey.speaker.tempo = 120
codey.speaker.volume = 20
codey.speaker.play_note("C4", 1)
codey.speaker.rest(1)
```

## `sound_sensor` &mdash; 板载音量传感器

**功能相关函数**

`sound_sensor.get_loudness()`<br/>
获得音量传感器检测的声音强度, 返回值是音量的大小。 数值范围 `0 ~ 100`。

**程序示例**

```py
import codey

while True:
    codey.display.show(codey.sound_sensor.get_loudness())
```

## `light_sensor` &mdash; 板载光线传感器

**功能相关函数**

`light_sensor.get_value()`<br/>
获得光线传感器检测的光线强度, 返回值是可见光的强度值。 数值范围 `0 ~ 100`。

**程序示例**

```py
import codey

while True:
    codey.display.show(codey.light_sensor.get_value())
```

## `potentiometer` &mdash; 板载电位器旋钮

**功能相关函数**

`potentiometer.get_value()`<br/>
获得电位器旋钮的当前数值。 数值范围 `0 ~ 100`。

**程序示例**

```py
import codey

while True:
    codey.display.show(codey.potentiometer.get_value())
```

## `button_a` &mdash; 板载按键A

**功能相关函数**

`button_a.is_pressed()`<br/>
获取按键A当前状态。 返回的结果是 `True`：按键被按下， 或者 `False`: 按键未被按下。

**程序示例**

```py
import codey

def loop():
    while True:
        if codey.button_a.is_pressed():
            print("button A is pressed")
loop()
```

## `button_b` &mdash; 板载按键B

**功能相关函数**

`button_b.is_pressed()`<br/>
获取按键B当前状态。 返回的结果是 `True`：按键被按下， 或者 `False`: 按键未被按下。

**程序示例**

```py
import codey

def loop():
    while True:
        if codey.button_b.is_pressed():
            print("button B is pressed")
loop()
```

## `button_c` &mdash; 板载按键C

**功能相关函数**

`button_c.is_pressed()`<br/>
获取按键C当前状态。 返回的结果是 `True`：按键被按下， 或者 `False`: 按键未被按下。

**程序示例**

```py
import codey

def loop():
    while True:
        if codey.button_c.is_pressed():
            print("button C is pressed")
loop()
```

## `motion_sensor` &mdash; 板载姿态传感器

**姿态传感器说明**

![](codey-gyroscope.png)

如上图所示，roll，pitch（翻滚角，俯仰角）的方向以右手螺旋定则为标准。

小程水平放置时roll和pitch都为 `0°`
- roll的范围： `-90° ~ 90°`
- pitch的范围： `-180° ~ 180°`

**功能相关函数**

`motion_sensor.get_roll()`<br/>
获取姿态角的翻滚角，返回的数据范围是 `-90 ~ 90`

`motion_sensor.get_pitch()`<br/>
获取姿态角的俯仰角，返回的数据范围是 `-180 ~ 180`

`motion_sensor.get_yaw()`<br/>
获取姿态角的偏航角，返回的数据范围是 `0 ~ 360`，由于小程板载的传感器是六轴传感器，没有电子罗盘。所以实际上偏航角只是使用了Z轴角速度的积分。它存在着积累误差。如果是想获得真实偏航角的，这个API不适合使用。

`motion_sensor.get_rotation(axis)`<br/>
获得小程在三个轴上转动的角度，以逆时针转动方向为正方向，参数：
- *axis* 字符串类型，以 `x`，`y`，`z` 代表小程定义的坐标轴。

`motion_sensor.reset_rotation(axis = "all")`<br/>
初始化绕三个轴转动的当前角度为0，`get_rotation()` 函数将从 0 开始计算，参数：
- *axis* 字符串类型，以 `x`，`y`，`z` 代表小程定义的坐标轴, `all` 代表全部的三个轴。也是这个函数的默认值。

`motion_sensor.is_shaked()`<br/>
检测小程是否有被摇晃，返回值是布尔值，其中 `True` 表示小程被晃动了，`False` 表示小程未被晃动。

`motion_sensor.get_shake_strength()`<br/>
如果小程被摇晃了，这个函数可以获得摇晃的强度，返回值的数值范围是 `0 ~ 100`， 数值越大，晃动的强度就越大。

`motion_sensor.is_tilted_left()`<br/>
检测小程是否向左倾斜，返回值是布尔值，其中 `True` 表示小程向左倾斜了，`False` 表示小程未向左倾斜。

`motion_sensor.is_tilted_right()`<br/>
检测小程是否向右倾斜，返回值是布尔值，其中 `True` 表示小程向右倾斜了，`False` 表示小程未向右倾斜。

`motion_sensor.is_ears_up()`<br/>
检测小程是否耳朵向上，返回值是布尔值，其中 `True` 表示小程耳朵朝上，`False` 表示小程耳朵没有朝上。

`motion_sensor.is_ears_down()`<br/>
检测小程是否耳朵向下，返回值是布尔值，其中 `True` 表示小程耳朵朝下，`False` 表示小程耳朵没有朝下。

`motion_sensor.is_display_up()`<br/>
检测小程是否表情面板朝上，返回值是布尔值，其中 `True` 表示小程表情面板朝上，`False` 表示小程表情面板没有朝上。

`motion_sensor.is_display_down()`<br/>
检测小程是否表情面板朝下，返回值是布尔值，其中 `True` 表示小程表情面板朝下，`False` 表示小程表情面板没有朝下。

`motion_sensor.is_upright()`<br/>
检测小程是否直立，返回值是布尔值，其中 `True` 表示小程直立，`False` 表示小程没有直立。

`motion_sensor.get_acceleration(axis)`<br/>
获取三个轴的加速度值，单位是 `m/s^2`，参数：
- *axis* 字符串类型，以 `x`，`y`，`z` 代表小程定义的坐标轴。

`motion_sensor.get_gyroscope(axis)`<br/>
获取三个轴的角速度值，单位是 `°/`秒，参数：
- *axis* 字符串类型，以 `x`，`y`，`z` 代表小程定义的坐标轴。

**程序示例1：**

```py
import codey
import time

while True:
    roll = codey.motion_sensor.get_roll()
    pitch = codey.motion_sensor.get_pitch()
    yaw = codey.motion_sensor.get_yaw()
    print("roll:", end = "")
    print(roll, end = "")
    print("   ,pitch:", end = "")
    print(pitch, end = "")
    print("   ,yaw:", end = "")
    print(yaw)
    time.sleep(0.05)
```
**程序示例2：**

```py
import codey

while True:
    if codey.motion_sensor.is_shaked():
        print("shake_strength:", end = "")
        print(codey.motion_sensor.get_shake_strength())
```
**程序示例3：**

```py
import codey

while True:
    if codey.motion_sensor.is_tilted_left():
        print("tilted_left")
    if codey.motion_sensor.is_tilted_right():
        print("tilted_right")
    if codey.motion_sensor.is_ears_up():
        print("ears_up")
    if codey.motion_sensor.is_ears_down():
        print("ears_down")
    if codey.motion_sensor.is_display_up():
        print("display_up")
    if codey.motion_sensor.is_display_down():
        print("display_down")
    if codey.motion_sensor.is_upright():
        print("upright")
```

## `ir` &mdash; 板载红外收发

**功能相关函数**

`ir.receive()`<br/>
返回红外收到的字符串信息，所以发送端发送的数据必须以 `\n` 结束。如果是接收 NEC编码协议的遥控器指令，请使用另外一个函数 `receive_remote_code()`。

`ir.receive_remote_code()`<br/>
获取红外遥控器数据，红外遥控器数据包含地址和内容两部分，因此返回一个长度为2的list数据。 前面一个参数是地址码，后面一个参数是数据码。

`ir.send(str)`<br/>
发送红外字符串，参数：
- *str* 要发射的字符串数据，`send` 函数会在字符串末尾自动加入 `\n` 结束符。

`ir.start_learning()`<br/>
开始红外学习，仅支持学习标准NEC协议的遥控器指令。

`ir.stop_learning()`<br/>
停止红外学习

`ir.save_learned_result(index)`<br/>
将学习的红外编码结果保存到相应区域，参数：
- *index* 数值范围是 `0 ~ 15`，一共有16个存储区域。

`ir.send_learned_result(index = 1)`<br/>
发送红外学习保存下来的红外编码， 默认发送 index = 1的区域的学习结果，参数：
- *index* 数值范围是 `0 ~ 15`，一共有16个存储区域。

`ir.learn(time = 3)`<br/>
红外学习 `time` 秒，在调用该API后会保存 `time` 秒内学到的红外信息。 默认会保留到index = 1的区域，参数：
- *time* 学习时间，单位是 `秒`。

**程序示例1：**

```py
import codey
import event

@event.start
def start_cb():
    print("start event succeeded")
    while True:
        codey.display.show(codey.ir.receive_remote_code()[1])
```

**程序示例2：**

```py
import codey
import event

@event.button_a_pressed
def button_a_cb():
    print("button a event succeeded")
    codey.ir.learn()
    codey.led.show(0, 100, 0)

@event.button_b_pressed
def button_a_cb():
    print("button b event succeeded")
    while True:
        codey.ir.send_learned_result()

@event.button_c_pressed
def button_c_cb():
    print("button b event succeeded")
    while True:
        codey.display.show(codey.ir.receive())
```

## `wifi` &mdash; 板载Wi-Fi

**功能相关函数**

`wifi.start(ssid = "wifi_ssid", password = "password", mode = codey.wifi.STA)`<br/>
启动wifi连接，该API不阻塞，API退出不代表Wi-Fi已连接上，需要调用 `wifi.is_connected()` 判断，参数：
- *ssid* 字符串类型，Wi-Fi账号。
- *password* 字符串类型，Wi-Fi密码。
- *mode* 启动Wi-Fi的模式。

`wifi.is_connected()`<br/>
检测wifi是否已连接上，返回值是布尔值，其中 `True` 表示Wi-Fi已经建立连接，`False` 表示wifi尚未建立连接。

**常量**

`wifi.STA`<br/>
Wi-Fi的站点模式，即无线网卡模式，该模式下，Wi-Fi可以连接到路由器。

`wifi.AP`<br/>
Wi-Fi的无线接入点模式，一般的无线路由/网桥工作在该模式，该模式下，Wi-Fi可以允许其它无线设备接入。

`wifi.APSTA`<br/>
Wi-Fi的AP和STA模式共存。

**程序示例**

```py
import codey
codey.wifi.start('makeblock', 'password', codey.wifi.STA)
codey.led.show(0,0,0)
while True:
    if codey.wifi.is_connected():
        codey.led.show(0,0,255)

    else:
        codey.led.show(0,0,0)
```

## `battery` &mdash; 内置锂电池

**功能相关函数**

`battery.get_voltage()`<br/>
获取当前的电池电压，返回值是一个浮点数据。单位是 `V`

`battery.get_percentage()`<br/>
获取剩余电池电量的百分比，返回值是一个整数，数据范围是 `0 ~ 100`，其中 100 表示剩余电量还有 100%。

**程序示例**

```py
import codey

while True:
    print("vol" + str(codey.battery.get_voltage()))
    print("percentage" + str(codey.battery.get_percentage()))
```

## `codey_timer` &mdash; 计数器

**功能相关函数**

`codey.get_timer()`<br/>
获取计时器当前值（计时器从用户脚本启动时开始运行），返回值是一个浮点数据，单位是 `秒`。

`codey.reset_timer()`<br/>
初始化计时器的值

**程序示例**

```py
import codey

codey.reset_timer()

while True:
    print("time:", end = "")
    print(codey.get_timer())
```

## `codey_broadcast` &mdash; 广播模块

**功能相关函数**

`codey.broadcast(str)`<br/>
可以向串口，蓝牙以及自身的事件监听单元发送一个广播，参数：
- `str` 广播的内容。

**程序示例**

```py
import codey
import event

@event.button_a_pressed
def button_a_cb():
    print("button a event succeeded")
    codey.broadcast("hello")

@event.received("hello")
def received_cb():
    print("received message: hello")
```

## `codey_external_module_detect` &mdash; 模块接入检测

**功能相关函数**

`codey.has_neuron_connected()`<br/>
检测是否有任何神经元模块接入小程，返回值是布尔值，其中 `True` 表示有神经元模块接入了小程(包括小奔的接入)，`False` 表示没有任何神经元模块的接入。

`codey.is_rocky_connected()`<br/>
检测小奔是否接入小程，返回值是布尔值，其中 `True` 表示有小奔接入了小程，`False` 表示小奔没有接入。

**程序示例**

```py
import codey
import time

while True:
    if codey.is_rocky_connected():
        print("rocky is in")
    else:
        print("rocky is out")
    time.sleep(1)
```

## `codey_script_control` &mdash; 脚本/线程控制

**功能相关函数**

`codey.stop_this_script()`<br/>
停止当前脚本。

`codey.stop_other_scripts()`<br/>
停止其他脚本。

`codey.stop_this_script()`<br/>
停止所有脚本。

**程序示例**

```py
import codey
import time
import event

@event.start
def start_cb():
    while True:
        print("start cb executing...")
        time.sleep(1)
        print("stop this script")
        codey.stop_this_script()

@event.button_a_pressed
def button_a_cb():
    codey.stop_other_scripts()
    while True:
        print("button a event")

@event.button_b_pressed
def button_b_cb():
    codey.stop_other_scripts()
    while True:
        print("button b event")

@event.button_c_pressed
def button_c_cb():
    codey.stop_all_scripts()
```