# 程小奔帮助文档

\* 如有任何技术疑问，请关注微信公众号：“Makeblock售后”

> 搜索微信号：`makeblock2013`  
> 或扫一扫  
> <img src="qr-code.jpg" width="100px;" style="padding:5px 5px 5px 0px;">

感谢您购买使用程小奔！

此帮助文档主要分为以下部分：

* [程小奔](README.md)
    * [简介](tutorials/introduction.md)
    * [快速上手](tutorials/getting-started.md)
    * [上传模式](tutorials/upload-mode.md)
    * [个性积木](tutorials/emotion-blocks.md)
    * [IoT](tutorials/iot.md)
    * [神经元](tutorials/neuron.md)
    * [红外遥控器](tutorials/ir.md)
    * [使用Python](tutorials/use-python.md)
* [案例](examples/examples.md)
* [积木说明](block-reference/block-reference.md)
* [Python API 文档](python-api/python-api.md)
* [FAQ](faq/faq.md)
