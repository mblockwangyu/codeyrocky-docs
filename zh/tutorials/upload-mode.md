# 上传模式

程小奔默认的编程模式是“上传模式”。当程小奔连接到慧编程后，“上传模式”默认启动，如下图：

![](images/upload-mode-1.png)

在“上传模式”下，编程有以下这些特性：

1. 所有程序必须上传到小程运行。完成编程后，点击”上传到设备“进行上传。
<br><br>
2. 程序可离线运行，与慧编程断开连接后仍会运行程序。
<br><br>
3. 关机再重新开机，程小奔仍会运行关机前最后上传的程序。
<br><br>
4. 在“上传模式”下，程小奔不能结合Scratch舞台进行编程（如要使用，请关闭“上传模式”。）

    所以，事件类积木有三个特定积木无法使用，分别是：<span style="background-color:#D1D3D5;color:white;padding:3px; font-size: 12px">当绿色旗帜被点击</span>、<span style="background-color:#D1D3D5;color:white;padding:3px; font-size: 12px">当按下（）键</span>、<span style="background-color:#D1D3D5;color:white;padding:3px; font-size: 12px">广播（）并等待</span>。

    ![](images/upload-mode-4.png)

    <div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;"><strong>注：</strong><br>
    事件类积木发送的广播只能用在项目内，并且，程小奔发送的广播只能用于自身。
</div>
<br>

## 关闭“上传模式”

“上传模式”关闭后，编程有以下这些特性：

1. 程序不需要上传到设备运行，无“上传到设备”按钮。
    
    ![](images/upload-mode-2.png)

    鼠标单击可直接运行程序，方便你检测程序效果。更改程序后，再次单击运行就可以看到新的效果。

    <div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;"><strong>注：</strong><br>
    鼠标单击运行程序，鼠标双击执行单个积木。
</div>
<br>

2. 程序不能离线运行。小程必须与慧编程保持连接才可运行程序。
<br><br>
3. 结合Scratch舞台进行编程。在“上传模式”下不可使用的事件类积木现可使用，分别是：<span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当绿色旗帜被点击</span>、<span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当按下（）键</span>、<span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">广播（）并等待</span>。

    需注意的是，关闭上传模式后，事件类积木 <span style="background-color:#D1D3D5;color:white;padding:3px; font-size: 12px">当小程启动</span> 不可使用。

    ![](images/upload-mode-3.png)

    <div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;"><strong>注：</strong><br>
    事件类积木发送的广播只能用在项目内，可以用于：程小奔给自身发送、程小奔与舞台之间互相通信、舞台内部通信。
</div>

