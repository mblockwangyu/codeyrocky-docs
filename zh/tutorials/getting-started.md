# 快速上手

本章将介绍通过慧编程快速使用程小奔，开启编程之旅。

## 连接程小奔

1\. 打开慧编程。使用USB数据线将小程连接到电脑（使用慧编程App请参照界面指导）。

![](connect-codey.png)

2\. 按下小程电源键开机，电源指示灯亮起。

![](power-on-codey.png)

3\. 选中“程小奔”，点击“连接”。在弹出的连接设备窗口，点击“连接”。

![](connect-1.png)

![](connect-2.png)

## 新建一个程小奔项目

让我们先从一个简单的项目开始吧。当我们摇晃小程时，小程会播放“哈喽”的声音。

1\. 从事件类积木拖取一个 <span style="background-color:#FFBF00;color:white;padding:3px;font-size:12px">当小程摇晃时</span> 积木到脚本区。

![](get-started-1.png)

2\. 从播放类积木拖取一个 <span style="background-color:#CF63CF;color:white;padding:3px;font-size:12px">播放声音（）</span> 积木，拼接在原来的积木上。

![](get-started-2.png)

3\. 点击”上传到设备“将程序上传到小程。

![](get-started-3.png)

4\. 试着摇晃一下小程吧！