# 个性积木

个性积木赋予程小奔”人格“魅力，让它会笑会闹会撒娇。

## 添加个性积木

1\. 在”设备“下，选中”程小奔“。点击积木区最下方的”+“按钮。

![](emotion-blocks-1.png)

2\. 在弹出的”扩展中心“页面，选择”个性“，点击”+“。

![](emotion-blocks-2.png)

3\. 返回编辑页。瞧，积木多了一种类型：个性。

![](emotion-blocks-3.png)

## 使用个性积木

让我们来试试新的个性积木吧。我们来制作一个小游戏，程小奔无意中走进一个山洞，洞里都是金币，它非常开心地将所有金币收入囊中。

开始编程前，要先将一些巧克力金币和玩具铁锅挂起来。

<p style="text-align:center"><video width="320" height="240" controls><source src="emotion-blocks.mp4" type="video/mp4"></video></p>
<br/>
\> <small>视频来自Jimmy Qin</small>

1\. 从事件类积木拖取一个 <span style="background-color:#FFBF00;color:white;padding:3px;font-size:12px;">当小程启动</span> 积木到脚本区，再添加一个运动类积木 <span style="background-color:#4C97FF;color:white;padding:3px;font-size:12px;">前进以动力（）%（）秒</span>，并将动力改为100%，时间设为0.5秒。

![](emotion-blocks-4.png)

![](emotion-blocks-5.png)

2\. 依次添加四个个性类积木：<span style="background-color:#A3D237;color:white;padding:3px;font-size:12px;">东张西望</span>、<span style="background-color:#A3D237;color:white;padding:3px;font-size:12px;">惊叹</span>、<span style="background-color:#A3D237;color:white;padding:3px;font-size:12px;">向左看</span>、<span style="background-color:#A3D237;color:white;padding:3px;font-size:12px;">向右看</span>。

![](emotion-blocks-6.png)

3\. 再添加一个运动积木 <span style="background-color:#4C97FF;color:white;padding:3px;font-size:12px;">前进以动力（）%（）秒</span>，并将动力改为100%，时间设为0.5秒。

![](emotion-blocks-7.png)

4\. 添加一个控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px;font-size:12px;">重复执行（）次</span>，设为4次。再添加个性类积木 <span style="background-color:#A3D237;color:white;padding:3px;font-size:12px;">肯定</span> 和运动类积木 <span style="background-color:#4C97FF;color:white;padding:3px;font-size:12px;">前进以动力（）%（）秒</span>，并将动力改为100%，时间设为0.2秒。

![](emotion-blocks-8.png)

![](emotion-blocks-9.png)

![](emotion-blocks-10.png)

5\. 继续添加两个个性类积木 <span style="background-color:#A3D237;color:white;padding:3px;font-size:12px;">得意</span> 和 <span style="background-color:#A3D237;color:white;padding:3px;font-size:12px;">笑</span>。

![](emotion-blocks-11.png)

6\. 再添加一个运动类积木 <span style="background-color:#4C97FF;color:white;padding:3px;font-size:12px;">前进以动力（）%（）秒</span>，并将动力改为100%，时间设为0.6秒。

![](emotion-blocks-12.png)

7\. 最后再添加三个个性类积木 <span style="background-color:#A3D237;color:white;padding:3px;font-size:12px;">吓一跳</span>、<span style="background-color:#A3D237;color:white;padding:3px;font-size:12px;">头晕</span> 和 <span style="background-color:#A3D237;color:white;padding:3px;font-size:12px;">委屈</span>。

![](emotion-blocks-13.png)

8\. 点击”上传到设备“将程序上传到小程。

![](get-started-3.png)

9\. 看看程小奔怎么收集金币吧！

<a href="collect-coins.mblock" download>点击下载源码</a>