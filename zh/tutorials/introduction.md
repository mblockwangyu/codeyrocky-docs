![](codey.png)

# 简介

程小奔以软硬件交互的方式，鼓励孩子在创作和游戏中学习编程。内部集成10余种可编程控制的电子模块，只需几行代码，就可让程小奔实现各种功能。借助慧编程，你可以体验如搭积木般简单的编程，也可以切换到Python代码模式，解锁更多功能。

![](codey-rocky-04.png)

**小程**：控制器<br/>
**小奔**：底盘

## 使用慧编程

结合慧编程软件，你可以通过编程让程小奔做各种各样的事情，比如显示LED点阵动画，演奏乐器，追光，等有趣的玩法。

慧编程软件有电脑端和移动端，也可以在网页上访问。

电脑端：[http://www.mblock.cc/mblock-software/](http://www.mblock.cc/mblock-software/)<br/>
安卓和iOS：请在应用商店搜索“慧编程”下载使用<br/>
网页端：[https://ide.makeblock.com/](https://ide.makeblock.com/)

**_注意：_**_此帮助文档的截图都为慧编程电脑端，仅供参考。但所有的示例在电脑端和移动端都能实现。_