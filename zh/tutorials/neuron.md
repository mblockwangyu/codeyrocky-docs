# 神经元积木

程小奔可以和神经元套件结合使用。神经元套件包含光线传感器，人体红外传感器，湿度传感器等30多个可编程的电子模块。将程小奔和神经元搭配使用，探索更多乐趣。

## 添加神经元积木

1\. 在”设备“下，选中”程小奔“。点击积木区最下方的”+“按钮。

![](emotion-blocks-1.png)

2\. 在弹出的”扩展中心“页面，选择”神经元“，点击”+“。

![](neuron-blocks-1.png)

3\. 返回编辑页。瞧，积木多了一种类型：神经元。

![](neuron-blocks-2.png)

## 使用神经元积木

让我们来试一下神经元积木吧！我们将使用神经元套件的LED灯带来创建一个新项目。

1\. 将LED灯带连接到小程。

2\. 从事件类积木拖取一个 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当按下按钮（）</span> 积木到脚本区。

![](neuron-blocks-3.png)

3\. 从神经元类积木拖取一个<span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">灯带（）显示（）</span> ，将灯带的颜色设置为如下重复的序列。

![](neuron-blocks-5.png)

![](neuron-blocks-4.png)

4\. 点击”上传到设备“将程序上传到小程。

![](get-started-3.png)

5\. 按下按钮A，看看灯带是不是亮起来啦！