# 红外遥控器

你可以定义自己专属的红外遥控器来控制程小奔。

## 添加红外遥控器积木

1\. 在”设备“下，选中”程小奔“。点击积木区最下方的”+“按钮。

![](emotion-blocks-1.png)

2\. 在弹出的”扩展中心“页面，选择”IoT“，点击”+“。

![](ir-blocks-1.png)

3\. 返回编辑页。瞧，积木多了一种类型：IoT。

![](ir-blocks-2.png)

## 使用红外遥控器积木

让我们来试试红外遥控积木吧！我们将新建一个项目，使用红外遥控器简单控制程小奔。

1\. 从事件类积木拖取一个 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当小程启动</span> 积木到脚本区。

![](ir-blocks-3.png)

2\. 当遥控器向上按钮按下时，我们想让程小奔往前走。我们需要从控制类积木拖取一个 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">如果（）那么（）</span> 积木，从红外遥控器类积木拖取一个 <span style="background-color:#FF6680;color:white;padding:3px; font-size: 12px">红外遥控器按下（）键？</span>，再从运动类积木拖取一个 <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">前进以动力（）%（）秒</span>。

![](ir-blocks-4.png)

![](ir-blocks-5.png)

![](ir-blocks-6.png)

3\. 当遥控器向下按钮按下时，我们想让程小奔后退。我们需要从控制类积木拖取一个 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">如果（）那么（）</span>  积木，从红外遥控器类积木拖取一个 <span style="background-color:#FF6680;color:white;padding:3px; font-size: 12px">红外遥控器按下（）键？</span> 积木，再从运动类积木拖取一个 <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">后退以动力（）%（）秒</span> 积木。

![](ir-blocks-7.png)

![](ir-blocks-8.png)

4\. 当遥控器向左按钮按下时，我们想让程小奔左转。我们需要从控制类积木拖取一个 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">如果（）那么（）</span> 积木，从红外遥控器类积木拖取一个<span style="background-color:#FF6680;color:white;padding:3px; font-size: 12px">红外遥控器按下（）键？</span> 积木，再从运动类积木拖取一个 <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">左转以动力（）%（）秒</span> 积木。

![](ir-blocks-9.png)

![](ir-blocks-10.png)

5\. 当遥控器向右按钮按下时，我们想让程小奔右转。我们需要从控制类积木拖取一个 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">如果（）那么（）</span> 积木，从红外遥控器类积木拖取一个 <span style="background-color:#FF6680;color:white;padding:3px; font-size: 12px">红外遥控器按下（）键？</span> 积木，再从运动类积木拖取一个 <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">右转以动力（）%（）秒</span> 积木。

![](ir-blocks-11.png)

![](ir-blocks-12.png)

6\. 添加一个控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">重复执行</span>。

<img src="images/ir.png" width="800px;" style="padding:5px 5px 20px 5px;">

7\. 点击”上传到设备“将程序上传到小程。

![](get-started-3.png)

8\. 按下红外遥控器的按键，让程小奔动起来吧！