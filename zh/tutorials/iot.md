# IoT （物联网）积木

IoT (Internet of Things)，就是大家熟知的物联网，它通过网络把各式各样的设备连在一起，进行数据的分享和通信。通过内置Wi-Fi，程小奔可以接入互联网，实现IoT功能，比如获取天气数据。使用IoT积木来探索程小奔的更多功能吧。

## 添加IoT积木

1\. 在“设备”下，选中“程小奔”。点击积木区最下方的“+”按钮。

![](emotion-blocks-1.png)

2\. 在弹出的“扩展中心”页面，选择“IoT”，点击“+ 添加”。

![](iot-blocks-1.png)

3\. 返回编辑页。瞧，积木多了一种类型：IoT。

![](iot-blocks-2.png)

## 使用IoT积木

让我们来试试新的IoT积木吧。我们新建一个项目，让程小奔获取天气预报。

1\. 从事件类积木拖取一个 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当小程启动</span> 积木到脚本区。

![](iot-blocks-3.png)

2\. 从IoT类积木拖取一个 <span style="background-color:#FD691D;color:white;padding:3px; font-size: 12px">连接到网络（）密码（）</span> 积木。输入无线网络名和密码。

![](iot-blocks-4.png)

3\. 从控制类积木拖取一个 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">如果（）那么（）</span> 积木，并添加一个IoT类积木 <span style="background-color:#FD691D;color:white;padding:3px; font-size: 12px">网络已连接？</span>。程小奔需要接入网络才能获取天气数据。

![](iot-blocks-5.png)

![](iot-blocks-6.png)

4\. 从显示类积木拖取一个 <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">显示（）</span> 积木，包裹在 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">如果（）那么（）</span> 积木里。我们想让程小奔显示天气，所以我们要从IoT类积木拖取一个<span style="background-color:#FD691D;color:white;padding:3px; font-size: 12px">地区 天气</span> 积木。输入需要显示天气的城市，从下拉菜单里选择对应城市，这里我们选择了深圳。

![](iot-blocks-7.png)

![](iot-blocks-8.png)

5\. 点击”上传到设备“将程序上传到小程。

![](get-started-3.png)

6\. 看下程小奔的屏幕，是不是可以看到天气预报了。