# 播放类积木

小程通过扬声器发出声音，扬声器是可以一种发出声音的输出设备，常用于各种可以发声的电子设备中。通过小程的扬声器，可以为小程编写音乐，添加提示音等。

## 1. 播放声音（）

通过小程的扬声器播放指定音效。点击可从下拉菜单选择音效，默认音效为”哈喽“。

![](images/speaker-1-1.png)


<table cellpadding="10px;" cellspacing="12px;" style="text-align:center;">
<tr><th colspan="7" style="border: 1px solid black;">音效</th></tr>

<tr><td style="border: 1px solid black;">哈喽</td> <td style="border: 1px solid black;">嗨</td> <td style="border: 1px solid black;">拜</td> <td style="border: 1px solid black;">耶</td> <td style="border: 1px solid black;">哇哦</td> <td style="border: 1px solid black;">笑声</td> <td style="border: 1px solid black;">哼唱</td></tr>

<tr><td style="border: 1px solid black;">难过</td> <td style="border: 1px solid black;">叹气</td> <td style="border: 1px solid black;">哼</td> <td style="border: 1px solid black;">生气</td> <td style="border: 1px solid black;">惊吓</td> <td style="border: 1px solid black;">撒娇</td> <td style="border: 1px solid black;">好奇</td></tr>

<tr><td style="border: 1px solid black;">尴尬</td> <td style="border: 1px solid black;">准备</td> <td style="border: 1px solid black;">冲刺</td> <td style="border: 1px solid black;">打呼</td> <td style="border: 1px solid black;">喵</td> <td style="border: 1px solid black;">启动</td> <td style="border: 1px solid black;">开关</td></tr>

<tr><td style="border: 1px solid black;">哔哔</td> <td style="border: 1px solid black;">蜂鸣</td> <td style="border: 1px solid black;">排气</td> <td style="border: 1px solid black;">爆炸</td> <td style="border: 1px solid black;">获取</td> <td style="border: 1px solid black;">痛苦</td> <td style="border: 1px solid black;">跳动</td></tr>

<tr><td style="border: 1px solid black;">激光</td> <td style="border: 1px solid black;">升级</td> <td style="border: 1px solid black;">低能量</td> <td style="border: 1px solid black;">金属音</td> <td style="border: 1px solid black;">提示</td> <td style="border: 1px solid black;">正确</td> <td style="border: 1px solid black;">错误</td></tr>

<tr><td style="border: 1px solid black;">铃声</td><td style="border: 1px solid black;">得分</td> <td style="border: 1px solid black;">发射</td> <td style="border: 1px solid black;">脚步声1</td> <td style="border: 1px solid black;">脚步声2</td> <td style="border: 1px solid black;">激活</td> <td style="border: 1px solid black;">警告</td> </tr>

</table>

<br/>

**示例：**

![](images/speaker-1-2.png)

按下小程的按钮A时，小程将播放”哈喽“音效。

---

## 2. 播放声音（）直到结束

通过小程的扬声器播放指定音效并等待声音播放完毕。点击可从下拉菜单选择音效，默认音效为”哈喽“。

![](images/speaker-2-1.png)

**示例：**

![](images/speaker-2-2.png)

按下小程的按钮A时，小程将播放”哈喽“音效直到声音播放完毕。

---

## 3. 停止播放声音

停止小程扬声器发出的声音。

![](images/speaker-3-1.png)

**示例：**

![](images/speaker-3-2.png)

 按下小程的按钮A时，小程将播放”哈喽“音效十次，然后停止。

 ---

 ## 4. 播放音符（）以（）拍

 通过小程的扬声器播放指定节拍的音符。点击从下拉菜单选择音符，输入节拍数，默认为“C4”和0.25。

![](images/speaker-4-1.png)

 **示例：**

![](images/speaker-4-2.png)

按下小程的按钮A时，小程将播放音符C4，持续0.25拍。

---

## 5. 休止（）拍

停止小程扬声器发出的声音，等待指定拍数。点击可更改，默认为0.25拍。

![](images/speaker-5-1.png)

**示例：**

![](images/speaker-5-2.png)

按下小程的按钮A时，小程将播放连续5次音符C4，每次0.25拍，等待0.25拍后，播放5次音符G4，每次0.25拍。

---

## 6. 播放声音以频率（）赫兹，持续（）秒

通过小程的扬声器播放指定频率的声音，在指定时间之后停止。默认频率为700赫兹，持续1秒。

![](images/speaker-6-1.png)

**示例：**

![](images/speaker-6-2.png)

按下小程的按钮A时，小程将播放频率为700赫兹的声音1秒。

---

## 7. 将音量增加（）

改变小程扬声器的音量。填入负值减小音量，填入正值增加音量，默认数值为-10。

![](images/speaker-7-1.png)

**示例：**

![](images/speaker-7-2.png)

按下小程的按钮A时，小程扬声器的音量将减少10。

---

## 8. 将音量设置为（）%

以百分比设定小程扬声器的音量。有效值为0-100，默认为100%。

![](images/speaker-8-1.png)

**示例：**

![](images/speaker-8-2.png)

当小程启动时，扬声器的音量为60%。

---

## 9. 音量

报告小程扬声器的音量。

![](images/speaker-9-1.png)

**示例：**

![](images/speaker-9-2.png)

按下小程的按钮A时，小程屏幕会显示当前音量。

---

