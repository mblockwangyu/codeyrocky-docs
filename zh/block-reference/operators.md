# 运算类积木

## 1. （）+（）

执行加法运算。

![](images/operators-1-1.png)

**示例：**

![](images/operators-1-2.png)

按下小程按钮A，屏幕显示2加3的计算结果。

---

## 2. （）-（）

执行减法运算。

![](images/operators-2-1.png)

**示例：**

![](images/operators-2-2.png)

按下小程按钮A，屏幕显示3减1的计算结果。

---

## 3.（）*（）

执行乘法运算。

![](images/operators-3-1.png)

**示例：**

![](images/operators-3-2.png)

按下小程按钮A，屏幕显示2乘以3的计算结果。

---

## 4. （）/（）

执行除法运算。

![](images/operators-4-1.png)

**示例：**

![](images/operators-4-2.png)

按下小程按钮A，屏幕显示6除以2的计算结果。

---

## 5. 在（）和（）之间取随机数

在指定区间内取随机数。

![](images/operators-5-1.png)

**示例：**

![](images/operators-5-2.png)

按下小程按钮A，屏幕显示指定图案，时间为1和10之间的随机数秒。

---

## 6. （）>（）

如果指定参数的值大于指定值，报告条件成立。

![](images/operators-6-1.png)

**示例：**

![](images/operators-6-2.png)

摇晃小程，当摇晃程度大于10时，小程播放音效“哇哦”。

---

## 7. （）<（）

如果指定参数的值小于指定值，报告条件成立。

![](images/operators-7-1.png)

**示例：**

![](images/operators-7-2.png)

小程启动后，如果剩余电量小于50%，屏幕显示“low battery”。

---

## 8. （）=（）

如果指定参数的值等于指定值，报告条件成立。

![](images/operators-8-1.png)

**示例：**

![](images/operators-8-2.png)

小程启动后，如果小奔检测到的红色色值为255，小程的LED灯亮起红色。

---

## 9. （）与（）

指定两个条件同时成立，报告条件成立。

![](images/operators-9-1.png)

**示例：**

![](images/operators-9-2.png)

小程启动后，以50%动力前进，如果检测到红色物体，停止运动。

---

## 10. （）或（）

指定两个条件其中一个成立，报告条件成立。

![](images/operators-10-1.png)

**示例：**

![](images/operators-10-2.png)

小程启动后，按下按钮A或摇晃小程，小程都会播放音效“哈喽”。

---

## 11. （）不成立

指定条件不成立，报告条件成立。

![](images/operators-11-1.png)

**示例：**

![](images/operators-11-2.png)

小程启动后，只要小奔颜色红外传感器没有检测到物体，程小奔持续以50%动力前进。

---

## 12. 连接（）和（）

报告两个字符串合并结果。

![](images/operators-12-1.png)

**示例：**

![](images/operators-12-2.png)

小程启动后，屏幕依次显示“hi”、“morning”。

---

## 13. （）的第（）个字符

报告指定字符串的指定位置字符。

![](images/operators-13-1.png)

**示例：**

![](images/operators-13-2.png)

小程启动后，屏幕显示“morning”的第3个字符“r”。

---

## 14. （）的字符数

报告指定字符串的字符数。

![](images/operators-14-1.png)

**示例：**

![](images/operators-14-2.png)

小程启动后，屏幕显示“morning”的字符数。

---

## 15. （）包含（）？

如果指定字符串包含另一指定字符串，报告条件成立。

![](images/operators-15-1.png)

**示例：**

![](images/operators-15-2.png)

小程启动后，如果“苹果”包含“果”，屏幕显示“yes”。

---

## 16. （）除以（）的余数

报告指定两数相除的余数。

![](images/operators-16-1.png)

**示例：**

![](images/operators-16-2.png)

小程启动后，屏幕显示9除以6的余数。

---

## 17. 四舍五入（）

报告指定数字四舍五入的值。

![](images/operators-17-1.png)

**示例：**

![](images/operators-17-2.png)

小程启动后，屏幕显示10.7四舍五入的结果。

---

## 18. （）（）

报告指定数字的指定数学运算结果，包括绝对值、向下取整、向上取整、平方根、sin、cos、tan、asin、acos、atan、ln、log、e^、10^，共15选项。

![](images/operators-18-1.png)

**示例：**

![](images/operators-18-2.png)

小程启动后，屏幕显示16的平方根。

---



