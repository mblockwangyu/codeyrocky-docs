# 红外通讯类积木

## 1. 发送红外消息（）

发送指定红外消息。

![](images/infrared-1-1.png)

**示例：**

![](images/infrared-1-2.png)

按下小程按钮A，小程将发送红外消息A。

---

## 2. 接收的红外消息

报告接收到的红外消息。

![](images/infrared-2-1.png)

---

## 3. 录制家电遥控器信号3秒

录制家电遥控器信号，时间为3秒。

![](images/infrared-3-1.png)

**示例：**

![](images/infrared-3-2.png)

按下小程按钮A，程小奔会录制家电遥控信号3秒。

---

## 4. 发送家电遥控器信号

发送录制的家电遥控器信号。

![](images/infrared-4-1.png)

**示例：**

![](images/infrared-4-2.png)

按下小程按钮A，程小奔将发送家电遥控器信号。

---