# 灯光类积木

**RGB LED指示灯**

![](codey-led.png)

小程的屏幕下方，有一个RGB LED指示灯。RGB LED指示灯由三颗LED组成，它们分别为红色、绿色和蓝色。通过控制三种颜色的亮度，可以混合出各种你想要的颜色。

![](rocky-led.png)

小奔的颜色红外传感器中央有一颗RGB LED指示灯，由三颗LED组成，它们分别为红色、绿色和蓝色。通过控制三种颜色的点亮与熄灭，可以混合出七种颜色。小奔的RGB LED指示灯不可调节亮度。

## 1. 指示灯亮起（）（）秒

小程的RGB LED指示灯亮起指定颜色，并在指定时间后熄灭。点击进行更改，默认颜色为红色，时间为1秒。

![](images/lighting-1-1.png)

**示例：**

![](images/lighting-1-2.png)

按下小程的按钮A时，小程的指示灯会亮起红色，并在1秒后熄灭。

---

## 2. 指示灯亮起（）

小程的RGB LED指示灯亮起指定颜色。默认颜色为红色。

![](images/lighting-2-1.png)

**示例：**

![](images/lighting-2-2.png)

按下小程的按钮A时，小程的指示灯会亮起红色。

---

## 3. 设置指示灯（）色值为（）

设置小程指示灯指定颜色的色值，包括红、绿、蓝三个选项。点击进行更改，默认颜色是红色，色值为255。

![](images/lighting-3-1.png)

执行单个颜色的积木时，不会改变指示灯余下两种颜色的色值。

**参数范围：**
- 红：0 ~ 255
- 绿：0 ~ 255
- 蓝：0 ~ 255

**示例：**

![](images/lighting-3-2.png)

按下小程的按钮A时，小程的指示灯会亮起黄色。

---

## 4. 熄灭指示灯

熄灭小程的RGB LED指示灯。

![](images/lighting-4-1.png)

**示例：**

![](images/lighting-4-2.png)

按下小程的按钮A时，小程的指示灯会亮起黄色，并在3秒后熄灭。

---

## 5. 设置小奔的灯光颜色为（）

设置底盘小奔的LED为指定颜色，有红、绿、蓝、黄、青、紫、白七个选项。点击在下拉菜单选择颜色，默认颜色为红色。

![](images/lighting-5-1.png)

**示例：**

![](images/lighting-5-2.png)

按下小程的按钮A时，小奔的指示灯会亮起蓝色。

---

## 6. 熄灭小奔的灯光

熄灭底盘小奔的LED。

![](images/lighting-6-1.png)

**示例：**

![](images/lighting-6-2.png)

按下小程的按钮A时，小奔的指示灯会亮起蓝色，并在2秒后熄灭。