# 积木说明

本章节包含了每个积木类别的详细介绍。

* [显示类](looks.md)
* [灯光类](lighting.md)
* [播放类](speaker.md)
* [运动类](action.md)
* [感知类](sensing.md)
* [红外通讯类](infrared.md)
* [事件类](events.md)
* [控制类](control.md)
* [运算类](operators.md)