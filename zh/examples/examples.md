![](codey-rocky-product-image.jpg)

> 更新日期：2018/8/15
> 
> 适用软件及版本：mblock V5.0.0-beta.4

# 程小奔案例

## 1. 用按钮切换小程的表情

1\) 拖取一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当按下按钮（）</span> 到脚本区，保留默认值。

![](project-1-1.png)

2\) 添加一个播放类积木 <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">播放声音（）</span>，选择“开关”。

![](project-1-2.png)

3\) 添加一个显示类积木 <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">显示图案（）</span>，保留默认图案。

![](project-1-3.png)

4\) 点击”上传到设备“将程序上传到小程。

![](upload.png)

5\) 按下按钮A看看小程的表情吧！

## 2. 用小程制作点阵动画

1\) 拖取一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当小程摇晃时</span> 积木到脚本区。

![](project-2-1.png)

2\) 从显示类积木拖取一个 <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">显示图案（）（）秒</span> 积木，并绘制以下图案。将时间设为0.3秒。

![](project-2-3.png)

![](project-2-2.png)

3\) 添加一个显示类积木 <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">显示图案（）（）秒</span>，绘制以下图案。将时间设为0.3秒。

![](project-2-4.png)

![](project-2-5.png)

4\) 再添加一个显示类积木 <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">显示图案（）（）秒</span>，绘制以下图案。将时间设为0.3秒。

![](project-2-6.png)

![](project-2-7.png)

5\) 从控制类积木拖取一个 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">重复执行（）次</span> 积木将三块显示积木包裹起来，将次数设为6。

![](project-2-8.png)

6\) 添加一点音效，让我们从播放类积木拖取一个 <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">播放声音（）</span> 积木，将声音设为跳动。

![](project-2-10.png)

7\) 点击”上传到设备“将程序上传到小程。

![](upload.png)

8\) 摇晃一下小程试试看！

![](project-2-9.png)

## 3. 让小程播放音乐

1\) 拖取一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当按下按钮（）</span> 到脚本区，保留默认值。

![](project-1-1.png)

2\) 添加四个播放类积木 <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">播放音符（）以（）拍</span>，将音符分别设为C4、E4、F4、A5。

![](project-3-2.png)

**音量控制程序**

3\) 添加一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当小程启动</span>。

![](project-3-3.png)

4\) 添加一个播放类积木 <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">将音量设置为（）%</span> 和一个感知类积木 <span style="background-color:#4CBFE6;color:white;padding:3px; font-size: 12px">齿轮电位器读数</span>，这样我们就可以控制音量大小。

![](project-3-4.png)

![](project-3-5.png)

5\) 再添加一个控制积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">重复执行</span>，我们才能随时调节音量。

![](project-3-6.png)

6\) 点击”上传到设备“将程序上传到小程。

![](upload.png)

7\) 按下按钮A让小程演奏音乐吧！

## 4. 让小程眨眼

1\) 拖取一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当小程启动</span> 到脚本区。

![](project-4-1.png)

2\) 添加一个显示类积木 <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">显示图案（）（）秒</span> 和一个运算类积木 <span style="background-color:#59C059;color:white;padding:3px; font-size: 12px">在（）和（）之间取随机数</span>，将数值更改为2和5，让小程随机眨眼。

![](project-4-2.png)

![](project-4-3.png)

3\) 再添加一个显示类积木 <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">显示图案（）（）秒</span>，绘制下列图案，并将时间改为0.2。

![](project-4-4.png)

![](project-4-5.png)

4\) 添加一个控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">重复执行</span>，让小程重复眨眼睛。

![](project-4-6.png)

5\) 点击”上传到设备“将程序上传到小程。

![](upload.png)

6\) 看看小程眨眼睛吧！

## 5. 让小程识别颜色

1\) 拖取一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当小程启动</span> 到脚本区。

![](project-4-1.png)

2\) 添加一个控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">如果（）那么（）</span> 和一个感知类积木 <span style="background-color:#4CBFE6;color:white;padding:3px; font-size: 12px">检测到的颜色是（）？</span>，保留默认红色。

![](project-5-1.png)

![](project-5-2.png)

3\) 添加一个灯光类积木 <span style="background-color:#B080D5;color:white;padding:3px; font-size: 12px">指示灯亮起（）</span>，当检测到红色时，程小奔指示灯会亮起红色。添加一个播放类积木 <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">播放声音（）</span>，选择“得分”，添加一些音效。

![](project-5-3.png)

![](project-5-4.png)

4\) 我们想让程小奔同样检测蓝色，右键点击复制检测红色的脚本，将颜色更改为蓝色。

![](project-5-5.png)

![](project-5-6.png)

5\) 再添加一个控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">重复执行</span>，让程序始终执行。

![](project-5-7.png)

6\) 点击”上传到设备“将程序上传到小程。

![](upload.png)

7\) 试一试吧！

**注意：将颜色红外传感器转至下方。**

![](project-5-8.png)

## 6. 让小程躲避障碍物

1\) 拖取一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当小程启动</span> 到脚本区。

![](project-4-1.png)

2\) 添加一个显示类积木 <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">显示图案（）</span>，保留默认图案。

![](project-6-1.png)

3\) 添加一个运动类积木 <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">（）以动力（）%</span>，选择“前进”，一个控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">等待（）</span>，和一个感知类积木 <span style="background-color:#4CBFE6;color:white;padding:3px; font-size: 12px">检测到物体？</span>。

![](project-6-2.png)

![](project-6-3.png)

![](project-6-4.png)

4\) 当程小奔检测到障碍物时，我们让它后退并右转。我们需要三个运动类积木：<span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">停止运动</span>，<span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">后退以动力（）%（）秒</span>，和 <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">右转以动力（）%（）秒</span>。

![](project-6-5.png)

5\) 再添加一个控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">重复执行</span>，让程序始终执行。

![](project-6-6.png)

6\) 点击”上传到设备“将程序上传到小程。

![](upload.png)

7\) 看看程小奔躲避障碍物吧！

**注意：将红外传感器转动至前方。**

## 7. 让小程控制舞台演奏乐器

这个项目结合了舞台编程。

开始创作前，请点击关闭上传模式。如下所示：

![](disable-upload-mode.png)

1\) 选中“设备”下的程 小奔，拖取一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当按下按钮（）</span> 到脚本区，保留默认按钮A。

![](project-7-1.png)

2\) 当按钮按下时，我们想要舞台角色接收到消息。我们需要添加一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">广播（）</span>，新建一个消息并命名为“A”。

![](project-7-2.png)

![](project-7-3.png)

**舞台程序**

3\) 选中“角色”，点击“+”添加角色。在弹出的角色库页面，选择“Drum”并点击“确定”。

![](project-7-4.png)

![](project-7-5.png)

4\) 选中“Drum”，拖取一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当接收到（）</span>，一个声音类积木 <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">播放声音（）</span>，保持默认的“high tom”。

![](project-7-6.png)

![](project-7-8.png)

![](project-7-7.png)

5\) 为“Drum”添加一些动画效果。添加一个外观类积木 <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">换成（）造型</span>，选择“drum-b”，一个控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">等待（）秒</span>，时间设为0.2秒，再添加一个外观类积木 <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">换成（）造型</span>，保持默认“drum-a”。

![](project-7-9.png)

![](project-7-10.png)

![](project-7-11.png)

6\) 保存你的项目，按下按钮A看看吧！

## 8. 设计程小奔的控制台

这个项目结合了舞台编程。

开始创作前，请点击关闭上传模式。如下所示：

![](disable-upload-mode.png)

**舞台程序**

1\) 选择“角色”，点击“&times;”删除默认角色熊猫。点击“+”添加新角色，在角色库中选择“Arrow1”，点击“确认”。

![](project-8-1.png)

![](project-8-2.png)

2\) 选择“Arrow1”，添加事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当角色被点击</span> 和 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">广播（）</span>，新建消息“Right”。

![](project-8-3.png)

![](project-8-4.png)

3\) 右键点击“Arrow1”进行复制，新角色会被自动命名为“Arrow2”，点击“造型”，选择第二个造型，完成后点击“&times;”完成设置。在脚本区，点击新建消息“Left”。

![](project-8-5.png)

![](project-8-6.png)

![](project-8-7.png)

![](project-8-8.png)

4\) 重复步骤3，添加另一角色“Arrow3”，新建消息“Down”。

![](project-8-10.png)

![](project-8-11.png)

5\) 重复步骤3，再添加一个角色“Arrow4”，新建消息“Up”。

![](project-8-12.png)

![](project-8-13.png)

6\) 点击调整箭头位置，如下图所示。

![](project-8-14.png)

**程小奔程序**

7\) 选中“设备”下的“程小奔”，添加一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当接收到（）</span>，选择“Left”，和一个运动类积木 <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">左转以动力（）%（）秒</span>，保留默认值。

![](project-8-15.png)

![](project-8-16.png)

8\) 添加一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当接收到（）</span>，选择“Right”，和一个运动类积木 <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">右转以动力（）%（）秒</span>，保留默认值。

![](project-8-17.png)

9\) 添加一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当接收到（）</span>，选择“Up”，和一个运动类积木 <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">前进以动力（）%（）秒</span>，保留默认值。

![](project-8-18.png)

10\) 添加一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当接收到（）</span>，选择“Up”，和一个运动类积木 <span style="background-color:#4C97FF;color:white;padding:3px; font-size: 12px">后退以动力（）%（）秒</span>，保留默认值。

![](project-8-19.png)

11\) 保存你的项目，点击舞台上的箭头控制程小奔吧！

## 9. 用程小奔制作一个数字炸弹

1\) 拖取一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当小程启动</span> 到脚本区。

![](project-4-1.png)

2\) 选择变量类积木，点击“建立一个变量”，将变量命名为“Number”，点击“确定”。

![](project-9-1.png)

![](project-9-2.png)

3\) 添加一个变量类积木 <span style="background-color:#FF8C1A;color:white;padding:3px; font-size: 12px">将Number设为（）</span>，保留默认值0.

![](project-9-3.png)

4\) 添加一个显示类积木 <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">显示（）直到滚动结束</span>，和一个变量类积木 <span style="background-color:#FF8C1A;color:white;padding:3px; font-size: 12px">Number</span>。

![](project-9-4.png)

![](project-9-5.png)

5\) 添加一个事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当按下按钮（）</span>，保留默认按钮A，和一个变量类积木 <span style="background-color:#FF8C1A;color:white;padding:3px; font-size: 12px">将Number增加（）</span>，保留默认值。

![](project-9-6.png)

![](project-9-7.png)

6\) 为游戏增添音效，添加一个播放类积木 <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">播放声音（）</span>，选择“开关”。

![](project-9-8.png)

7\) 右击复制积木，并添加到 <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">播放声音（）</span> 积木后面。

![](project-9-9.png)

![](project-9-10.png)

8\) 设置游戏结束规则，添加一个控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">如果（）那么（）</span> 。

![](project-9-11.png)

9\) 从运算类积木拖取一个 <span style="background-color:#59C059;color:white;padding:3px; font-size: 12px">（）&gt;（）</span> 积木和一个 <span style="background-color:#59C059;color:white;padding:3px; font-size: 12px">在（）和（）之间取随机数</span>，再从变量类积木拖取一个 <span style="background-color:#FF8C1A;color:white;padding:3px; font-size: 12px">Number</span> 积木。

![](project-9-12.png)

![](project-9-13.png)

10\) 添加一个显示类积木 <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">显示图案（）</span> ，点击绘制如下图案。再添加一个播放类积木 <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">播放声音（）</span>，选择“爆炸”。

![](project-9-14.png)

![](project-9-15.png)

![](project-9-16.png)

11\) 点击”上传到设备“将程序上传到小程。

![](upload.png)

12\) 试一试吧！

# 更多资源

## 示例程序

慧编程本身自带了丰富的示例程序，您可以在作品管理页直接获取。

![](examples.png)

## 教育站

您还可以登陆官方教育站，获得更多学习资源：

请访问：[http://education.makeblock.com/zh-hans/resource/](http://education.makeblock.com/zh-hans/resource/)

## 论坛分享

欢迎大家登陆程小奔的官方论坛分享自己的作品，交流学习经验：<br/>
[http://bbs.makeblock.com/forum-95-1.html](http://bbs.makeblock.com/forum-95-1.html)